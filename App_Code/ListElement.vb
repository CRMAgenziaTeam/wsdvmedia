﻿Public Class ListElement

    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Private _id As Integer
	
    Public Property count() As Integer
        Get
            Return _count
        End Get
        Set(value As Integer)
            _count = value
        End Set
    End Property
    Private _count As Integer
	
	Public Property proposto() As Integer
        Get
            Return _proposto
        End Get
        Set(value As Integer)
            _proposto = value
        End Set
    End Property
    Private _proposto As Integer
	
    Public Property data_inserimento() As Date
        Get
            Return _data_inserimento
        End Get
        Set(value As Date)
            _data_inserimento = value
        End Set
    End Property
    Private _data_inserimento As date
	
	Public Property data_inserimento_string() As String
        Get
            Return _data_inserimento_string
        End Get
        Set(value As String)
            _data_inserimento_string = value
        End Set
    End Property
    Private _data_inserimento_string As String
	
    Public Property data_modifica() As Date
        Get
            Return _data_modifica
        End Get
        Set(value As Date)
            _data_modifica = value
        End Set
    End Property
    Private _data_modifica As Date
	
	Public Property data_modifica_string() As String
        Get
            Return _data_modifica_string
        End Get
        Set(value As String)
            _data_modifica_string = value
        End Set
    End Property
    Private _data_modifica_string As String
    
	Public Property idreferente() As integer
        Get
            Return _idreferente
        End Get
        Set(value As Integer)
            _idreferente = value
        End Set
    End Property
    Private _idreferente As Integer

    Public Property idconsulente() As integer
        Get
            Return _idconsulente
        End Get
        Set(value As integer)
            _idconsulente = value
        End Set
    End Property
    Private _idconsulente As Integer
	
	Public Property consulente() As String
        Get
            Return _consulente
        End Get
        Set(value As String)
            _consulente = value
        End Set
    End Property
    Private _consulente As String

    Public Property idacquisitore() As Integer
        Get
            Return _idacquisitore
        End Get
        Set(value As integer)
            _idacquisitore = value
        End Set
    End Property
    Private _idacquisitore As integer
	
	Public Property acquisitore() As String
        Get
            Return _acquisitore
        End Get
        Set(value As String)
            _acquisitore = value
        End Set
    End Property
    Private _acquisitore As String
	
	Public Property filiale() As String
        Get
            Return _filiale
        End Get
        Set(value As String)
            _filiale = value
        End Set
    End Property
    Private _filiale As String

    Public Property idagenzia() As integer
        Get
            Return _idagenzia
        End Get
        Set(value As integer)
            _idagenzia = value
        End Set
    End Property
    Private _idagenzia As Integer

    Public Property tipotabella() As String
        Get
            Return _tipotabella
        End Get
        Set(value As String)
            _tipotabella = value
        End Set
    End Property
    Private _tipotabella As String

    Public Property riferimento() As String
        Get
            Return _riferimento
        End Get
        Set(value As String)
            _riferimento = value
        End Set
    End Property
    Private _riferimento As String

    Public Property tipologia() As String
        Get
            Return _tipologia
        End Get
        Set(value As String)
            _tipologia = value
        End Set
    End Property
    Private _tipologia As String

    Public Property tipologia2() As String
        Get
            Return _tipologia2
        End Get
        Set(value As String)
            _tipologia2 = value
        End Set
    End Property
    Private _tipologia2 As String

    Public Property tipologia3() As String
        Get
            Return _tipologia3
        End Get
        Set(value As String)
            _tipologia3 = value
        End Set
    End Property
    Private _tipologia3 As String

    Public Property prezzo() As integer
        Get
            Return _prezzo
        End Get
        Set(value As Integer)
            _prezzo = value
        End Set
    End Property
    Private _prezzo As Integer

    Public Property superficie() As integer
        Get
            Return _superficie
        End Get
        Set(value As integer)
            _superficie = value
        End Set
    End Property
    Private _superficie As integer


    Public Property locali() As integer
        Get
            Return _locali
        End Get
        Set(value As integer)
            _locali = value
        End Set
    End Property
    Private _locali As Integer

    Public Property camere() As integer
        Get
            Return _camere
        End Get
        Set(value As integer)
            _camere = value
        End Set
    End Property
    Private _camere As integer

    Public Property bagni() As integer
        Get
            Return _bagni
        End Get
        Set(value As integer)
            _bagni = value
        End Set
    End Property
    Private _bagni As Integer

    Public Property classe_energetica() As String
        Get
            Return _classe_energetica
        End Get
        Set(value As String)
            _classe_energetica = value
        End Set
    End Property
    Private _classe_energetica As String

    Public Property comune() As String
        Get
            Return _comune
        End Get
        Set(value As String)
            _comune = value
        End Set
    End Property
    Private _comune As String


    Public Property area() As String
        Get
            Return _area
        End Get
        Set(value As String)
            _area = value
        End Set
    End Property
    Private _area As String

    Public Property area2() As String
        Get
            Return _area2
        End Get
        Set(value As String)
            _area2 = value
        End Set
    End Property
    Private _area2 As String

    Public Property area3() As String
        Get
            Return _area3
        End Get
        Set(value As String)
            _area3 = value
        End Set
    End Property
    Private _area3 As String

    Public Property latitudine() As String
        Get
            Return _latitudine
        End Get
        Set(value As String)
            _latitudine = value
        End Set
    End Property
    Private _latitudine As String

    Public Property longitudine() As String
        Get
            Return _longitudine
        End Get
        Set(value As String)
            _longitudine = value
        End Set
    End Property
    Private _longitudine As String

    Public Property piano() As String
        Get
            Return _piano
        End Get
        Set(value As String)
            _piano = value
        End Set
    End Property
    Private _piano As String

    Public Property manutenzione() As String
        Get
            Return _manutenzione
        End Get
        Set(value As String)
            _manutenzione = value
        End Set
    End Property
    Private _manutenzione As String

    Public Property foto() As String
        Get
            Return _foto
        End Get
        Set(value As String)
            _foto = value
        End Set
    End Property
    Private _foto As String

    Public Property foto_thumb() As String
        Get
            Return _foto_thumb
        End Get
        Set(value As String)
            _foto_thumb = value
        End Set
    End Property
    Private _foto_thumb As String

    Public Property affinity() As String
        Get
            Return _affinity
        End Get
        Set(value As String)
            _affinity = value
        End Set
    End Property
    Private _affinity As String

    Public Property autobox() As String
        Get
            Return _autobox
        End Get
        Set(value As String)
            _autobox = value
        End Set
    End Property
    Private _autobox As String

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String
	
	Public Property note() As String
        Get
            Return _note
        End Get
        Set(value As String)
            _note = value
        End Set
    End Property
    Private _note As String

    Public Property zona() As String
        Get
            Return _zona
        End Get
        Set(value As String)
            _zona = value
        End Set
    End Property
    Private _zona As String

    Public Property spese_extra() As String
        Get
            Return _spese_extra
        End Get
        Set(value As String)
            _spese_extra = value
        End Set
    End Property
    Private _spese_extra As String

    Public Property spese_sospeso() As String
        Get
            Return _spese_sospeso
        End Get
        Set(value As String)
            _spese_sospeso = value
        End Set
    End Property
    Private _spese_sospeso As String

    Public Property valore_mercato() As String
        Get
            Return _valore_mercato
        End Get
        Set(value As String)
            _valore_mercato = value
        End Set
    End Property
    Private _valore_mercato As String

    Public Property infoplus() As String
        Get
            Return _infoplus
        End Get
        Set(value As String)
            _infoplus = value
        End Set
    End Property
    Private _infoplus As String

    Public Property cell_scheda() As String
        Get
            Return _cell_scheda
        End Get
        Set(value As String)
            _cell_scheda = value
        End Set
    End Property
    Private _cell_scheda As String

    Public Property telefono_scheda() As String
        Get
            Return _telefono_scheda
        End Get
        Set(value As String)
            _telefono_scheda = value
        End Set
    End Property
    Private _telefono_scheda As String
	
    Public Property email_scheda() As String
        Get
            Return _email_scheda
        End Get
        Set(value As String)
            _email_scheda = value
        End Set
    End Property
    Private _email_scheda As String

    Public Property nome_scheda() As String
        Get
            Return _nome_scheda
        End Get
        Set(value As String)
            _nome_scheda = value
        End Set
    End Property
    Private _nome_scheda As String

    Public Property telefono_referente() As String
        Get
            Return _telefono_referente
        End Get
        Set(value As String)
            _telefono_referente = value
        End Set
    End Property
    Private _telefono_referente As String

    Public Property indirizzo_referente() As String
        Get
            Return _indirizzo_referente
        End Get
        Set(value As String)
            _indirizzo_referente = value
        End Set
    End Property
    Private _indirizzo_referente As String

    Public Property email_referente() As String
        Get
            Return _email_referente
        End Get
        Set(value As String)
            _email_referente = value
        End Set
    End Property
    Private _email_referente As String

    Public Property cell_referente() As String
        Get
            Return _cell_referente
        End Get
        Set(value As String)
            _cell_referente = value
        End Set
    End Property
    Private _cell_referente As String

    Public Property sito_referente() As String
        Get
            Return _sito_referente
        End Get
        Set(value As String)
            _sito_referente = value
        End Set
    End Property
    Private _sito_referente As String

    Public Property nome_referente() As String
        Get
            Return _nome_referente
        End Get
        Set(value As String)
            _nome_referente = value
        End Set
    End Property
    Private _nome_referente As String

    Public Property descrizione_interna() As String
        Get
            Return _descrizione_interna
        End Get
        Set(value As String)
            _descrizione_interna = value
        End Set
    End Property
    Private _descrizione_interna As String

    Public Property descrizione_breve() As String
        Get
            Return _descrizione_breve
        End Get
        Set(value As String)
            _descrizione_breve = value
        End Set
    End Property
    Private _descrizione_breve As String

    Public Property riscaldamento() As String
        Get
            Return _riscaldamento
        End Get
        Set(value As String)
            _riscaldamento = value
        End Set
    End Property
    Private _riscaldamento As String

    Public Property disponibilita() As String
        Get
            Return _disponibilita
        End Get
        Set(value As String)
            _disponibilita = value
        End Set
    End Property
    Private _disponibilita As String

    Public Property condominio() As String
        Get
            Return _condominio
        End Get
        Set(value As String)
            _condominio = value
        End Set
    End Property
    Private _condominio As String

    Public Property RenditaCatastale() As String
        Get
            Return _RenditaCatastale
        End Get
        Set(value As String)
            _RenditaCatastale = value
        End Set
    End Property
    Private _RenditaCatastale As String

    Public Property tipoincarico() As String
        Get
            Return _tipoincarico
        End Get
        Set(value As String)
            _tipoincarico = value
        End Set
    End Property
    Private _tipoincarico As String

    Public Property categoria() As String
        Get
            Return _categoria
        End Get
        Set(value As String)
            _categoria = value
        End Set
    End Property
    Private _categoria As String

    Public Property valore_Stima() As String
        Get
            Return _valore_Stima
        End Get
        Set(value As String)
            _valore_Stima = value
        End Set
    End Property
    Private _valore_Stima As String

    Public Property indirizzo() As String
        Get
            Return _indirizzo
        End Get
        Set(value As String)
            _indirizzo = value
        End Set
    End Property
    Private _indirizzo As String

	Public Property status() As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property
    Private _status As String
	
	Public Property immobile() As String
        Get
            Return _immobile
        End Get
        Set(value As String)
            _immobile = value
        End Set
    End Property
    Private _immobile As String

	Public Property cliente_delegato() As String
        Get
            Return _cliente_delegato
        End Get
        Set(value As String)
            _cliente_delegato = value
        End Set
    End Property
    Private _cliente_delegato As String

	Public Property cliente_delegato_telefono() As String
        Get
            Return _cliente_delegato_telefono
        End Get
        Set(value As String)
            _cliente_delegato_telefono = value
        End Set
    End Property
    Private _cliente_delegato_telefono As String
	
	Public Property cliente_delegato_cellulare() As String
        Get
            Return _cliente_delegato_cellulare
        End Get
        Set(value As String)
            _cliente_delegato_cellulare = value
        End Set
    End Property
    Private _cliente_delegato_cellulare As String

	Public Property cliente_delegato_email() As String
        Get
            Return _cliente_delegato_email
        End Get
        Set(value As String)
            _cliente_delegato_email = value
        End Set
    End Property
    Private _cliente_delegato_email As String
	
	Public Property opzioniDatabase() As String
        Get
            Return _opzioniDatabase
        End Get
        Set(value As String)
            _opzioniDatabase = value
        End Set
    End Property
    Private _opzioniDatabase As String


End Class
