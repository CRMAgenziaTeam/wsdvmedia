﻿Imports Microsoft.VisualBasic

Public Class Obiettivo

	Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Private _id As Integer
	
	Public Property iddvm() As Integer
        Get
            Return _iddvm
        End Get
        Set(value As Integer)
            _iddvm = value
        End Set
    End Property
    Private _iddvm As Integer

	Public Property idTipoAppuntamento() As Integer
        Get
            Return _idTipoAppuntamento
        End Get
        Set(value As Integer)
            _idTipoAppuntamento = value
        End Set
    End Property
    Private _idTipoAppuntamento As Integer
	
	Public Property obiettivo() As Integer
        Get
            Return _obiettivo
        End Get
        Set(value As Integer)
            _obiettivo = value
        End Set
    End Property
    Private _obiettivo As Integer
	
	
	
End Class
