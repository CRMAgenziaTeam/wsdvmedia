Imports Microsoft.VisualBasic

Public Class PortalePromozione
    Public Property indice() As Integer
        Get
            Return _indice
        End Get
        Set(value As Integer)
            _indice = value
        End Set
    End Property
    Private _indice As Integer

    Public Property Midia() As String
        Get
            Return _midia
        End Get
        Set(value As String)
            _midia = value
        End Set
    End Property
    Private _midia As String

End Class