﻿Public Class Plus

    Public Property valore() As String
        Get
            Return _valore
        End Get
        Set(value As String)
            _valore = value
        End Set
    End Property
    Private _valore As String

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String

    Public Property gruppo() As String
        Get
            Return _gruppo
        End Get
        Set(value As String)
            _gruppo = value
        End Set
    End Property
    Private _gruppo As String

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property
    Private _id As String

End Class
