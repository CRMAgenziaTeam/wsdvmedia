﻿Imports Microsoft.VisualBasic

Public Class Bacheca

    Public Property Visitati() As String
        Get
            Return _Visti
        End Get
        Set(value As String)
            _Visti = value
        End Set
    End Property
    Private _Visti As String

    Public Property Preferiti() As String
        Get
            Return _Preferiti
        End Get
        Set(value As String)
            _Preferiti = value
        End Set
    End Property
    Private _Preferiti As String

    Public Property Consigliati() As String
        Get
            Return _Consigliati
        End Get
        Set(value As String)
            _Consigliati = value
        End Set
    End Property
    Private _Consigliati As String

    Public Property Affinity() As String
        Get
            Return _Affinity
        End Get
        Set(value As String)
            _Affinity = value
        End Set
    End Property
    Private _Affinity As String

End Class
