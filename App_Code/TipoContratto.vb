﻿Imports Microsoft.VisualBasic

Public Class TipoContratto

    Public Property TipoContratto() As String
        Get
            Return _TipoContratto
        End Get
        Set(value As String)
            _TipoContratto = value
        End Set
    End Property
    Private _TipoContratto As String

End Class
