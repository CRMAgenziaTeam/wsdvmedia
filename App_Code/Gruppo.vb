Imports Microsoft.VisualBasic

Public Class Gruppo
    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Private _id As Integer
	
	Public Property idDVm() As Integer
        Get
            Return _idDVm
        End Get
        Set(value As Integer)
            _idDVm = value
        End Set
    End Property
    Private _idDVm As Integer
	
	Public Property filiale() As String
        Get
            Return _filiale
        End Get
        Set(value As String)
            _filiale = value
        End Set
    End Property
    Private _filiale As String
	
	Public Property nominativo() As String
        Get
            Return _nominativo
        End Get
        Set(value As String)
            _nominativo = value
        End Set
    End Property
    Private _nominativo As String
	
	Public Property delegato() As String
        Get
            Return _delegato
        End Get
        Set(value As String)
            _delegato = value
        End Set
    End Property
    Private _delegato As String
	
	Public Property gruppo() As Integer
        Get
            Return _gruppo
        End Get
        Set(value As Integer)
            _gruppo = value
        End Set
    End Property
    Private _gruppo As Integer
	
	
End Class