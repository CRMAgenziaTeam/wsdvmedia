﻿Public Class Agenzia

    Public Property id_agenzia() As String
        Get
            Return _id_agenzia
        End Get
        Set(value As String)
            _id_agenzia = value
        End Set
    End Property
    Private _id_agenzia As String

    Public Property nome() As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property
    Private _nome As String

    Public Property indirizzo() As String
        Get
            Return _indirizzo
        End Get
        Set(value As String)
            _indirizzo = value
        End Set
    End Property
    Private _indirizzo As String

    Public Property indirizzo_breve() As String
        Get
            Return _indirizzo_breve
        End Get
        Set(value As String)
            _indirizzo_breve = value
        End Set
    End Property
    Private _indirizzo_breve As String

    Public Property comune_breve() As String
        Get
            Return _comune_breve
        End Get
        Set(value As String)
            _comune_breve = value
        End Set
    End Property
    Private _comune_breve As String

    Public Property latitudine() As String
        Get
            Return _latitudine
        End Get
        Set(value As String)
            _latitudine = value
        End Set
    End Property
    Private _latitudine As String

    Public Property longitudine() As String
        Get
            Return _longitudine
        End Get
        Set(value As String)
            _longitudine = value
        End Set
    End Property
    Private _longitudine As String

    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(value As String)
            _telefono = value
        End Set
    End Property
    Private _telefono As String

    Public Property cellulare() As String
        Get
            Return _cellulare
        End Get
        Set(value As String)
            _cellulare = value
        End Set
    End Property
    Private _cellulare As String

    Public Property fax() As String
        Get
            Return _fax
        End Get
        Set(value As String)
            _fax = value
        End Set
    End Property
    Private _fax As String

    Public Property email() As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property
    Private _email As String

    Public Property img_link_1() As String
        Get
            Return _img_link_1
        End Get
        Set(value As String)
            _img_link_1 = value
        End Set
    End Property
    Private _img_link_1 As String

    Public Property img_logo_1() As String
        Get
            Return _img_logo_1
        End Get
        Set(value As String)
            _img_logo_1 = value
        End Set
    End Property
    Private _img_logo_1 As String

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String

    Public Property link_sito() As String
        Get
            Return _linksito
        End Get
        Set(value As String)
            _linksito = value
        End Set
    End Property
    Private _linksito As String

    Public Property delegato() As String
        Get
            Return _delegato
        End Get
        Set(value As String)
            _delegato = value
        End Set
    End Property
    Private _delegato As String

    Public Property indirizzodelegato() As String
        Get
            Return _indirizzodelegato
        End Get
        Set(value As String)
            _indirizzodelegato = value
        End Set
    End Property
    Private _indirizzodelegato As String

    Public Property capdelegato() As String
        Get
            Return _capdelegato
        End Get
        Set(value As String)
            _capdelegato = value
        End Set
    End Property
    Private _capdelegato As String

    Public Property emaildelegato() As String
        Get
            Return _emaildelegato
        End Get
        Set(value As String)
            _emaildelegato = value
        End Set
    End Property
    Private _emaildelegato As String

    Public Property telefonodelegato() As String
        Get
            Return _telefonodelegato
        End Get
        Set(value As String)
            _telefonodelegato = value
        End Set
    End Property
    Private _telefonodelegato As String

    Public Property cellularedelegato() As String
        Get
            Return _cellularedelegato
        End Get
        Set(value As String)
            _cellularedelegato = value
        End Set
    End Property
    Private _cellularedelegato As String

    Public Property iban() As String
        Get
            Return _iban
        End Get
        Set(value As String)
            _iban = value
        End Set
    End Property
    Private _iban As String

    Public Property banca() As String
        Get
            Return _banca
        End Get
        Set(value As String)
            _banca = value
        End Set
    End Property
    Private _banca As String

    Public Property acquisitore() As String
        Get
            Return _acquisitore
        End Get
        Set(value As String)
            _acquisitore = value
        End Set
    End Property
    Private _acquisitore As String


End Class