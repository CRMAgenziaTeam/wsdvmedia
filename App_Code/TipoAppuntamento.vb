﻿Imports Microsoft.VisualBasic

Public Class TipoAppuntamento

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String

    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Private _id As Integer

    Public Property codice() As String
        Get
            Return _codice
        End Get
        Set(value As String)
            _codice = value
        End Set
    End Property
    Private _codice As String


End Class
