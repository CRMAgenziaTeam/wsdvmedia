﻿Imports Microsoft.VisualBasic

Public Class Azioni

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property
    Private _id As String

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String

End Class
