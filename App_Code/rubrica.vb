﻿Imports Microsoft.VisualBasic

Public Class rubrica

    Public Property iddvm() As String
        Get
            Return _iddvm
        End Get
        Set(value As String)
            _iddvm = value
        End Set
    End Property
    Private _iddvm As String

    Public Property idanagrafica() As String
        Get
            Return _idanagrafica
        End Get
        Set(value As String)
            _idanagrafica = value
        End Set
    End Property
    Private _idanagrafica As String
	
	Public Property idUtente() As String
        Get
            Return _idUtente
        End Get
        Set(value As String)
            _idUtente = value
        End Set
    End Property
    Private _idUtente As String

    Public Property colore() As String
        Get
            Return _colore
        End Get
        Set(value As String)
            _colore = value
        End Set
    End Property
    Private _colore As String

    Public Property nominativo() As String
        Get
            Return _nominativo
        End Get
        Set(value As String)
            _nominativo = value
        End Set
    End Property
    Private _nominativo As String
	
	Public Property nominativo_consulente() As String
        Get
            Return _nominativo_consulente
        End Get
        Set(value As String)
            _nominativo_consulente = value
        End Set
    End Property
    Private _nominativo_consulente As String
	
	Public Property nominativo_acquisitore() As String
        Get
            Return _nominativo_acquisitore
        End Get
        Set(value As String)
            _nominativo_acquisitore = value
        End Set
    End Property
    Private _nominativo_acquisitore As String

    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(value As String)
            _telefono = value
        End Set
    End Property
    Private _telefono As String

    Public Property telefono2() As String
        Get
            Return _telefono2
        End Get
        Set(value As String)
            _telefono2 = value
        End Set
    End Property
    Private _telefono2 As String
	
	Public Property telefono4() As String
        Get
            Return _telefono4
        End Get
        Set(value As String)
            _telefono4 = value
        End Set
    End Property
    Private _telefono4 As String
	
	Public Property telefono6() As String
        Get
            Return _telefono6
        End Get
        Set(value As String)
            _telefono6 = value
        End Set
    End Property
    Private _telefono6 As String

    Public Property tipo() As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property
    Private _tipo As String

    Public Property email() As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property
    Private _email As String

    Public Property note() As String
        Get
            Return _note
        End Get
        Set(value As String)
            _note = value
        End Set
    End Property
    Private _note As String
	
	Public Property note_richiesta() As String
        Get
            Return _note_richiesta
        End Get
        Set(value As String)
            _note_richiesta = value
        End Set
    End Property
    Private _note_richiesta As String

    Public Property indirizzo() As String
        Get
            Return _indirizzo
        End Get
        Set(value As String)
            _indirizzo = value
        End Set
    End Property
    Private _indirizzo As String

    Public Property cap() As String
        Get
            Return _cap
        End Get
        Set(value As String)
            _cap = value
        End Set
    End Property
    Private _cap As String

    Public Property filiale() As String
        Get
            Return _filiale
        End Get
        Set(value As String)
            _filiale = value
        End Set
    End Property
    Private _filiale As String

    Public Property latitudine() As String
        Get
            Return _latitudine
        End Get
        Set(value As String)
            _latitudine = value
        End Set
    End Property
    Private _latitudine As String

    Public Property longitudine() As String
        Get
            Return _longitudine
        End Get
        Set(value As String)
            _longitudine = value
        End Set
    End Property
    Private _longitudine As String

    Public Property data_rilevamento() As String
        Get
            Return _data_rilevamento
        End Get
        Set(value As String)
            _data_rilevamento = value
        End Set
    End Property
    Private _data_rilevamento As String

    Public Property acquisitore() As String
        Get
            Return _acquisitore
        End Get
        Set(value As String)
            _acquisitore = value
        End Set
    End Property
    Private _acquisitore As String

    Public Property consulente() As String
        Get
            Return _consulente
        End Get
        Set(value As String)
            _consulente = value
        End Set
    End Property
    Private _consulente As String

    Public Property UrlWeb() As String
        Get
            Return _UrlWeb
        End Get
        Set(value As String)
            _UrlWeb = value
        End Set
    End Property
    Private _UrlWeb As String

    Public Property Skype() As String
        Get
            Return _Skype
        End Get
        Set(value As String)
            _Skype = value
        End Set
    End Property
    Private _Skype As String

    Public Property budget() As String
        Get
            Return _budget
        End Get
        Set(value As String)
            _budget = value
        End Set
    End Property
    Private _budget As String

    Public Property delegato() As String
        Get
            Return _delegato
        End Get
        Set(value As String)
            _delegato = value
        End Set
    End Property
    Private _delegato As String

    Public Property indirizzodelegato() As String
        Get
            Return _indirizzodelegato
        End Get
        Set(value As String)
            _indirizzodelegato = value
        End Set
    End Property
    Private _indirizzodelegato As String

    Public Property capdelegato() As String
        Get
            Return _capdelegato
        End Get
        Set(value As String)
            _capdelegato = value
        End Set
    End Property
    Private _capdelegato As String

    Public Property emaildelegato() As String
        Get
            Return _emaildelegato
        End Get
        Set(value As String)
            _emaildelegato = value
        End Set
    End Property
    Private _emaildelegato As String

    Public Property telefonodelegato() As String
        Get
            Return _telefonodelegato
        End Get
        Set(value As String)
            _telefonodelegato = value
        End Set
    End Property
    Private _telefonodelegato As String

    Public Property cellularedelegato() As String
        Get
            Return _cellularedelegato
        End Get
        Set(value As String)
            _cellularedelegato = value
        End Set
    End Property
    Private _cellularedelegato As String

    Public Property Guser() As String
        Get
            Return _guser
        End Get
        Set(value As String)
            _guser = value
        End Set
    End Property
    Private _guser As String

    Public Property Gpassword() As String
        Get
            Return _gpassword
        End Get
        Set(value As String)
            _gpassword = value
        End Set
    End Property
    Private _gpassword As String

    Public Property Gcalendar() As String
        Get
            Return _gcalendar
        End Get
        Set(value As String)
            _gcalendar = value
        End Set
    End Property
    Private _gcalendar As String
	
	Public Property modPag() As String
        Get
            Return _modPag
        End Get
        Set(value As String)
            _modPag = value
        End Set
    End Property
    Private _modPag As String
	
	Public Property gruppo() As String
        Get
            Return _gruppo
        End Get
        Set(value As String)
            _gruppo = value
        End Set
    End Property
    Private _gruppo As String
	
	Public Property data_inserimento() As String
        Get
            Return _data_inserimento
        End Get
        Set(value As String)
            _data_inserimento = value
        End Set
    End Property
    Private _data_inserimento As String
	
	Public Property data_web() As String
        Get
            Return _data_web
        End Get
        Set(value As String)
            _data_web = value
        End Set
    End Property
    Private _data_web As String

End Class
