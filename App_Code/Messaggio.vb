Imports Microsoft.VisualBasic

Public Class Messaggio
    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Private _id As Integer
	
	Public Property idDVm() As Integer
        Get
            Return _idDVm
        End Get
        Set(value As Integer)
            _idDVm = value
        End Set
    End Property
    Private _idDVm As Integer
	
	Public Property idUtenteMittente() As Integer
        Get
            Return _idUtenteMittente
        End Get
        Set(value As Integer)
            _idUtenteMittente = value
        End Set
    End Property
    Private _idUtenteMittente As Integer
	
	Public Property idAppuntamento() As Integer
        Get
            Return _id_appuntamento
        End Get
        Set(value As Integer)
            _id_appuntamento = value
        End Set
    End Property
    Private _id_appuntamento As Integer
	
	Public Property idTipoAppuntamento() As Integer
        Get
            Return _id_tipo_appuntamento
        End Get
        Set(value As Integer)
            _id_tipo_appuntamento = value
        End Set
    End Property
    Private _id_tipo_appuntamento As Integer
	
	Public Property idTipoAppuntamentoSender As Integer
        Get
            Return _id_tipo_appuntamento_sender
        End Get
        Set(value As Integer)
            _id_tipo_appuntamento_sender = value
        End Set
    End Property
    Private _id_tipo_appuntamento_sender As Integer
	
	Public Property priorita() As Integer
        Get
            Return _priorita
        End Get
        Set(value As Integer)
            _priorita = value
        End Set
    End Property
    Private _priorita As Integer
	
	
	
	Public Property mittente() As String
        Get
            Return _mittente
        End Get
        Set(value As String)
            _mittente = value
        End Set
    End Property
    Private _mittente As String
	
	Public Property oggetto() As String
        Get
            Return _oggetto
        End Get
        Set(value As String)
            _oggetto = value
        End Set
    End Property
    Private _oggetto As String
	
	Public Property testo() As String
        Get
            Return _testo
        End Get
        Set(value As String)
            _testo = value
        End Set
    End Property
    Private _testo As String
	
	Public Property idImmobile() As Integer
        Get
            Return _idImmobile
        End Get
        Set(value As Integer)
            _idImmobile = value
        End Set
    End Property
    Private _idImmobile As Integer
	
	Public Property offerta() As String
        Get
            Return _offerta
        End Get
        Set(value As String)
            _offerta = value
        End Set
    End Property
    Private _offerta As String
	
	Public Property idRichiesta() As Integer
        Get
            Return _id_richiesta
        End Get
        Set(value As Integer)
            _id_richiesta = value
        End Set
    End Property
    Private _id_richiesta As Integer
	
	Public Property bloccato() As Integer
        Get
            Return _bloccato
        End Get
        Set(value As Integer)
            _bloccato = value
        End Set
    End Property
    Private _bloccato As Integer
	
	
	
	Public Property data_creazione() As Date
        Get
            Return _data_creazione
        End Get
        Set(value As Date)
            _data_creazione = value
        End Set
    End Property
    Private _data_creazione As Date
	
	Public Property data_creazione_string() As String
        Get
            Return _data_creazione_string
        End Get
        Set(value As String)
            _data_creazione_string = value
        End Set
    End Property
    Private _data_creazione_string As String
	
	
	Public Property data_notifica() As Date
        Get
            Return _data_notifica
        End Get
        Set(value As Date)
            _data_notifica = value
        End Set
    End Property
    Private _data_notifica As Date
	
	Public Property data_notifica_string() As String
        Get
            Return _data_notifica_string
        End Get
        Set(value As String)
            _data_notifica_string = value
        End Set
    End Property
    Private _data_notifica_string As String
    
    
	'AREA DESTINATARIO
	Public Property id_destinatario() As Integer
        Get
            Return _id_destinatario
        End Get
        Set(value As Integer)
            _id_destinatario = value
        End Set
    End Property
    Private _id_destinatario As Integer
	
	Public Property id_utente_destinatario() As Integer
        Get
            Return _id_utente_destinatario
        End Get
        Set(value As Integer)
            _id_utente_destinatario = value
        End Set
    End Property
    Private _id_utente_destinatario As Integer
	
	Public Property idTipo() As Integer
        Get
            Return _idTipo
        End Get
        Set(value As Integer)
            _idTipo = value
        End Set
    End Property
    Private _idTipo As Integer
	
	Public Property idCliente() As Integer
        Get
            Return _idCliente
        End Get
        Set(value As Integer)
            _idCliente = value
        End Set
    End Property
    Private _idCliente As Integer
	
	Public Property idStato() As Integer
        Get
            Return _idStato
        End Get
        Set(value As Integer)
            _idStato = value
        End Set
    End Property
    Private _idStato As Integer
	
	Public Property ricevuto() As Integer
        Get
            Return _ricevuto
        End Get
        Set(value As Integer)
            _ricevuto = value
        End Set
    End Property
    Private _ricevuto As Integer
	
	Public Property destinazione() As String
        Get
            Return _destinazione
        End Get
        Set(value As String)
            _destinazione = value
        End Set
    End Property
    Private _destinazione As String
	
	Public Property data_ricezione() As Date
        Get
            Return _data_ricezione
        End Get
        Set(value As Date)
            _data_ricezione = value
        End Set
    End Property
    Private _data_ricezione As Date
	
	Public Property data_ricezione_string() As String
        Get
            Return _data_ricezione_string
        End Get
        Set(value As String)
            _data_ricezione_string = value
        End Set
    End Property
    Private _data_ricezione_string As String
	
	
	
	Public Property foto_link() As String
        Get
            Return _foto_link
        End Get
        Set(value As String)
            _foto_link = value
        End Set
    End Property
    Private _foto_link As String
	
	Public Property idcliente_richiedente() As Integer
        Get
            Return _idcliente_richiedente
        End Get
        Set(value As Integer)
            _idcliente_richiedente = value
        End Set
    End Property
    Private _idcliente_richiedente As Integer
	
	Public Property nominativo_richiedente() As String
        Get
            Return _nominativo_richiedente
        End Get
        Set(value As String)
            _nominativo_richiedente = value
        End Set
    End Property
    Private _nominativo_richiedente As String
	
	Public Property telefono_richiedente() As String
        Get
            Return _telefono_richiedente
        End Get
        Set(value As String)
            _telefono_richiedente = value
        End Set
    End Property
    Private _telefono_richiedente As String
	
	Public Property cellulare_richiedente() As String
        Get
            Return _cellulare_richiedente
        End Get
        Set(value As String)
            _cellulare_richiedente = value
        End Set
    End Property
    Private _cellulare_richiedente As String
	
	Public Property email_richiedente() As String
        Get
            Return _email_richiedente
        End Get
        Set(value As String)
            _email_richiedente = value
        End Set
    End Property
    Private _email_richiedente As String
	
	
	Public Property immobile_idProprietario() As String
        Get
            Return _immobile_idProprietario
        End Get
        Set(value As String)
            _immobile_idProprietario = value
        End Set
    End Property
    Private _immobile_idProprietario As String
	
	Public Property immobile_zona() As String
        Get
            Return _immobile_zona
        End Get
        Set(value As String)
            _immobile_zona = value
        End Set
    End Property
    Private _immobile_zona As String
	
	Public Property immobile_indirizzo() As String
        Get
            Return _immobile_indirizzo
        End Get
        Set(value As String)
            _immobile_indirizzo = value
        End Set
    End Property
    Private _immobile_indirizzo As String
	
	Public Property immobile_prezzo() As String
        Get
            Return _immobile_prezzo
        End Get
        Set(value As String)
            _immobile_prezzo = value
        End Set
    End Property
    Private _immobile_prezzo As String
	
	
	
	
End Class