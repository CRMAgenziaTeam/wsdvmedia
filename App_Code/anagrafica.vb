﻿Imports Microsoft.VisualBasic

Public Class anagrafica

    Public Property iddvm() As String
        Get
            Return _iddvm
        End Get
        Set(value As String)
            _iddvm = value
        End Set
    End Property
    Private _iddvm As String

    Public Property idscheda() As String
        Get
            Return _idscheda
        End Get
        Set(value As String)
            _idscheda = value
        End Set
    End Property
    Private _idscheda As String
	
	Public Property idRichiesta() As Integer
        Get
            Return _idRichiesta
        End Get
        Set(value As Integer)
            _idRichiesta = value
        End Set
    End Property
    Private _idRichiesta As Integer

	Public Property nominativo() As String
        Get
            Return _nominativo
        End Get
        Set(value As String)
            _nominativo = value
        End Set
    End Property
    Private _nominativo As String

	
    Public Property nome() As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property
    Private _nome As String

    Public Property cognome() As String
        Get
            Return _cognome
        End Get
        Set(value As String)
            _cognome = value
        End Set
    End Property
    Private _cognome As String

    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(value As String)
            _telefono = value
        End Set
    End Property
    Private _telefono As String

    Public Property telefono2() As String
        Get
            Return _telefono2
        End Get
        Set(value As String)
            _telefono2 = value
        End Set
    End Property
    Private _telefono2 As String
	
	Public Property telefono4() As String
        Get
            Return _telefono4
        End Get
        Set(value As String)
            _telefono4 = value
        End Set
    End Property
    Private _telefono4 As String
	
	Public Property telefono6() As String
        Get
            Return _telefono6
        End Get
        Set(value As String)
            _telefono6 = value
        End Set
    End Property
    Private _telefono6 As String

    Public Property tipo_utente() As String
        Get
            Return _tipo_utente
        End Get
        Set(value As String)
            _tipo_utente = value
        End Set
    End Property
    Private _tipo_utente As String

    Public Property tipo_operatore() As String
        Get
            Return _tipo_operatore
        End Get
        Set(value As String)
            _tipo_operatore = value
        End Set
    End Property
    Private _tipo_operatore As String

    Public Property fascia_eta() As String
        Get
            Return _fascia_eta
        End Get
        Set(value As String)
            _fascia_eta = value
        End Set
    End Property
    Private _fascia_eta As String
	
	Public Property filiale() As String
        Get
            Return _filiale
        End Get
        Set(value As String)
            _filiale = value
        End Set
    End Property
    Private _filiale As String

    Public Property email() As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property
    Private _email As String
	
	Public Property email_del() As String
        Get
            Return _email_del
        End Get
        Set(value As String)
            _email_del = value
        End Set
    End Property
    Private _email_del As String

    Public Property password() As String
        Get
            Return _password
        End Get
        Set(value As String)
            _password = value
        End Set
    End Property
    Private _password As String

    Public Property comune_utente() As String
        Get
            Return _comune_utente
        End Get
        Set(value As String)
            _comune_utente = value
        End Set
    End Property
    Private _comune_utente As String

    Public Property indirizzo_utente() As String
        Get
            Return _indirizzo_utente
        End Get
        Set(value As String)
            _indirizzo_utente = value
        End Set
    End Property
    Private _indirizzo_utente As String

    Public Property regione() As String
        Get
            Return _regione
        End Get
        Set(value As String)
            _regione = value
        End Set
    End Property
    Private _regione As String

    Public Property provincia() As String
        Get
            Return _provincia
        End Get
        Set(value As String)
            _provincia = value
        End Set
    End Property
    Private _provincia As String

    Public Property area() As String
        Get
            Return _area
        End Get
        Set(value As String)
            _area = value
        End Set
    End Property
    Private _area As String

    Public Property note() As String
        Get
            Return _note
        End Get
        Set(value As String)
            _note = value
        End Set
    End Property
    Private _note As String
	
	Public Property noteRichiesta() As String
        Get
            Return _noteRichiesta
        End Get
        Set(value As String)
            _noteRichiesta = value
        End Set
    End Property
    Private _noteRichiesta As String
	
	Public Property esito As String
        Get
            Return _esito
        End Get
        Set(value As String)
            _esito = value
        End Set
    End Property
    Private _esito As String
	
    Public Property interesse As String
        Get
            Return _interesse
        End Get
        Set(value As String)
            _interesse = value
        End Set
    End Property
    Private _interesse As String
	
	Public Property delegato As String
        Get
            Return _delegato
        End Get
        Set(value As String)
            _delegato = value
        End Set
    End Property
    Private _delegato As String
    
	Public Property PartIva As String
        Get
            Return _PartIva
        End Get
        Set(value As String)
            _PartIva = value
        End Set
    End Property
    Private _PartIva As String
		
	Public Property ModPag As String
        Get
            Return _ModPag
        End Get
        Set(value As String)
            _ModPag = value
        End Set
    End Property
    Private _ModPag As String
	
	Public Property SaldoL As String
        Get
            Return _SaldoL
        End Get
        Set(value As String)
            _SaldoL = value
        End Set
    End Property
    Private _SaldoL As String
	
	Public Property SaldoE As String
        Get
            Return _SaldoE
        End Get
        Set(value As String)
            _SaldoE = value
        End Set
    End Property
    Private _SaldoE As String
	
	Public Property ceto As String
        Get
            Return _ceto
        End Get
        Set(value As String)
            _ceto = value
        End Set
    End Property
    Private _ceto As String
	
	Public Property professione As String
        Get
            Return _professione
        End Get
        Set(value As String)
            _professione = value
        End Set
    End Property
    Private _professione As String
	
	Public Property privacy_nom As String
        Get
            Return _privacy_nom
        End Get
        Set(value As String)
            _privacy_nom = value
        End Set
    End Property
    Private _privacy_nom As String
    
	Public Property ind_delegato As String
        Get
            Return _ind_delegato
        End Get
        Set(value As String)
            _ind_delegato = value
        End Set
    End Property
    Private _ind_delegato As String
	
	Public Property cap_del As String
        Get
            Return _cap_del
        End Get
        Set(value As String)
            _cap_del = value
        End Set
    End Property
    Private _cap_del As String
	
	Public Property privacy_del As String
        Get
            Return _privacy_del
        End Get
        Set(value As String)
            _privacy_del = value
        End Set
    End Property
    Private _privacy_del As String
	
	Public Property acquisitore As String
        Get
            Return _acquisitore
        End Get
        Set(value As String)
            _acquisitore = value
        End Set
    End Property
    Private _acquisitore As String
	
	Public Property consulente As Integer
        Get
            Return _consulente
        End Get
        Set(value As Integer)
            _consulente = value
        End Set
    End Property
    Private _consulente As Integer
	
	Public Property Guser As String
        Get
            Return _Guser
        End Get
        Set(value As String)
            _Guser = value
        End Set
    End Property
    Private _Guser As String
	
	Public Property Gpassword As String
        Get
            Return _Gpassword
        End Get
        Set(value As String)
            _Gpassword = value
        End Set
    End Property
    Private _Gpassword As String
	
	Public Property Gcalendar As String
        Get
            Return _Gcalendar
        End Get
        Set(value As String)
            _Gcalendar = value
        End Set
    End Property
    Private _Gcalendar As String
	
	Public Property documento As String
        Get
            Return _documento
        End Get
        Set(value As String)
            _documento = value
        End Set
    End Property
    Private _documento As String
	
	Public Property data_inserimento As String
        Get
            Return _data_inserimento
        End Get
        Set(value As String)
            _data_inserimento = value
        End Set
    End Property
    Private _data_inserimento As String
	
	Public Property data_nom As String
        Get
            Return _data_nom
        End Get
        Set(value As String)
            _data_nom = value
        End Set
    End Property
    Private _data_nom As String
	
	Public Property data_web As String
        Get
            Return _data_web
        End Get
        Set(value As String)
            _data_web = value
        End Set
    End Property
    Private _data_web As String
	
	Public Property data_word As String
        Get
            Return _data_word
        End Get
        Set(value As String)
            _data_word = value
        End Set
    End Property
    Private _data_word As String
	
	Public Property jolly As String
        Get
            Return _jolly
        End Get
        Set(value As String)
            _jolly = value
        End Set
    End Property
    Private _jolly As String
	
	Public Property cliente As String
        Get
            Return _cliente
        End Get
        Set(value As String)
            _cliente = value
        End Set
    End Property
    Private _cliente As String
	
	Public Property gruppo As String
        Get
            Return _gruppo
        End Get
        Set(value As String)
            _gruppo = value
        End Set
    End Property
    Private _gruppo As String
		
	Public Property attivo As String
        Get
            Return _attivo
        End Get
        Set(value As String)
            _attivo = value
        End Set
    End Property
    Private _attivo As String
	
	Public Property registro As String
        Get
            Return _registro
        End Get
        Set(value As String)
            _registro = value
        End Set
    End Property
    Private _registro As String
		
	Public Property azione As String
        Get
            Return _azione
        End Get
        Set(value As String)
            _azione = value
        End Set
    End Property
    Private _azione As String
	
	Public Property provincia_nascita As String
        Get
            Return _provincia_nascita
        End Get
        Set(value As String)
            _provincia_nascita = value
        End Set
    End Property
    Private _provincia_nascita As String
	
	Public Property skype As String
        Get
            Return _skype
        End Get
        Set(value As String)
            _skype = value
        End Set
    End Property
    Private _skype As String
	
	Public Property mutuo As String
        Get
            Return _mutuo
        End Get
        Set(value As String)
            _mutuo = value
        End Set
    End Property
    Private _mutuo As String
	
	Public Property disponibilita As String
        Get
            Return _disponibilita
        End Get
        Set(value As String)
            _disponibilita = value
        End Set
    End Property
    Private _disponibilita As String
	
	Public Property sitoweb As String
        Get
            Return _sitoweb
        End Get
        Set(value As String)
            _sitoweb = value
        End Set
    End Property
    Private _sitoweb As String
	
	Public Property azioniSocial As String
        Get
            Return _azioniSocial
        End Get
        Set(value As String)
            _azioniSocial = value
        End Set
    End Property
    Private _azioniSocial As String
	
	Public Property professioni As String
        Get
            Return _professioni
        End Get
        Set(value As String)
            _professioni = value
        End Set
    End Property
    Private _professioni As String
	
	Public Property specializzazioni As String
        Get
            Return _specializzazioni
        End Get
        Set(value As String)
            _specializzazioni = value
        End Set
    End Property
    Private _specializzazioni As String
		
	Public Property latitudine As String
        Get
            Return _latitudine
        End Get
        Set(value As String)
            _latitudine = value
        End Set
    End Property
    Private _latitudine As String
	
	Public Property longitudine As String
        Get
            Return _longitudine
        End Get
        Set(value As String)
            _longitudine = value
        End Set
    End Property
    Private _longitudine As String
	
	Public Property utenteInserimento As String
        Get
            Return _utenteInserimento
        End Get
        Set(value As String)
            _utenteInserimento = value
        End Set
    End Property
    Private _utenteInserimento As String
	
	Public Property utenteModifica As String
        Get
            Return _utenteModifica
        End Get
        Set(value As String)
            _utenteModifica = value
        End Set
    End Property
    Private _utenteModifica As String

End Class
