﻿Imports Microsoft.VisualBasic

Public Class Riscaldamento

	Public Property automatico() As Integer
        Get
            Return _automatico
        End Get
        Set(value As Integer)
            _automatico = value
        End Set
    End Property
    Private _automatico As Integer
	
	Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String
	
	Public Property iddvm() As Integer
        Get
            Return _iddvm
        End Get
        Set(value As Integer)
            _iddvm = value
        End Set
    End Property
    Private _iddvm As Integer

End Class
