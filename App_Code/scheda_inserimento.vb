﻿Imports Microsoft.VisualBasic

Public Class scheda_inserimento

    Public Property iddvm() As String
        Get
            Return _iddvm
        End Get
        Set(value As String)
            _iddvm = value
        End Set
    End Property
    Private _iddvm As String
	
	Public Property codiceOfferta() As String
	Get
            Return _codice_offerta
        End Get
        Set(value As String)
            _codice_offerta = value
        End Set
    End Property
    Private _codice_offerta As String
	
	Public Property idanagrafica() As String
        Get
            Return _idanagrafica
        End Get
        Set(value As String)
            _idanagrafica = value
        End Set
    End Property
    Private _idanagrafica As String
	
	Public Property filiale() As String
        Get
            Return _filiale
        End Get
        Set(value As String)
            _filiale = value
        End Set
    End Property
    Private _filiale As String

    Public Property idscheda() As String
        Get
            Return _idscheda
        End Get
        Set(value As String)
            _idscheda = value
        End Set
    End Property
    Private _idscheda As String
	
    Public Property nome() As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property
    Private _nome As String

    Public Property cognome() As String
        Get
            Return _cognome
        End Get
        Set(value As String)
            _cognome = value
        End Set
    End Property
    Private _cognome As String

    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(value As String)
            _telefono = value
        End Set
    End Property
    Private _telefono As String

    Public Property telefono2() As String
        Get
            Return _telefono2
        End Get
        Set(value As String)
            _telefono2 = value
        End Set
    End Property
    Private _telefono2 As String

    Public Property tipo_utente() As String
        Get
            Return _tipo_utente
        End Get
        Set(value As String)
            _tipo_utente = value
        End Set
    End Property
    Private _tipo_utente As String

    Public Property tipo_operatore() As String
        Get
            Return _tipo_operatore
        End Get
        Set(value As String)
            _tipo_operatore = value
        End Set
    End Property
    Private _tipo_operatore As String

    Public Property fascia_eta() As String
        Get
            Return _fascia_eta
        End Get
        Set(value As String)
            _fascia_eta = value
        End Set
    End Property
    Private _fascia_eta As String

    Public Property email() As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property
    Private _email As String

    Public Property password() As String
        Get
            Return _password
        End Get
        Set(value As String)
            _password = value
        End Set
    End Property
    Private _password As String

    Public Property comune_utente() As String
        Get
            Return _comune_utente
        End Get
        Set(value As String)
            _comune_utente = value
        End Set
    End Property
    Private _comune_utente As String

    Public Property indirizzo_utente() As String
        Get
            Return _indirizzo_utente
        End Get
        Set(value As String)
            _indirizzo_utente = value
        End Set
    End Property
    Private _indirizzo_utente As String

    Public Property categoria() As String
        Get
            Return _categoria
        End Get
        Set(value As String)
            _categoria = value
        End Set
    End Property
    Private _categoria As String

    Public Property contratto() As String
        Get
            Return _contratto
        End Get
        Set(value As String)
            _contratto = value
        End Set
    End Property
    Private _contratto As String

    Public Property tipologia() As String
        Get
            Return _tipologia
        End Get
        Set(value As String)
            _tipologia = value
        End Set
    End Property
    Private _tipologia As String

    Public Property classe_energetica() As String
        Get
            Return _classe_energetica
        End Get
        Set(value As String)
            _classe_energetica = value
        End Set
    End Property
    Private _classe_energetica As String

    Public Property manutenzione() As String
        Get
            Return _manutenzione
        End Get
        Set(value As String)
            _manutenzione = value
        End Set
    End Property
    Private _manutenzione As String

    Public Property epoca() As String
        Get
            Return _epoca
        End Get
        Set(value As String)
            _epoca = value
        End Set
    End Property
    Private _epoca As String

    Public Property esposizione() As String
        Get
            Return _esposizione
        End Get
        Set(value As String)
            _esposizione = value
        End Set
    End Property
    Private _esposizione As String

    Public Property mq() As String
        Get
            Return _mq
        End Get
        Set(value As String)
            _mq = value
        End Set
    End Property
    Private _mq As String
	
	Public Property mq_utili() As String
        Get
            Return _mq_utili
        End Get
        Set(value As String)
            _mq_utili = value
        End Set
    End Property
    Private _mq_utili As String

    Public Property area() As String
        Get
            Return _area
        End Get
        Set(value As String)
            _area = value
        End Set
    End Property
    Private _area As String

    Public Property zona() As String
        Get
            Return _zona
        End Get
        Set(value As String)
            _zona = value
        End Set
    End Property
    Private _zona As String

    Public Property indirizzo_scheda() As String
        Get
            Return _indirizzo_scheda
        End Get
        Set(value As String)
            _indirizzo_scheda = value
        End Set
    End Property
    Private _indirizzo_scheda As String

    Public Property piano() As String
        Get
            Return _piano
        End Get
        Set(value As String)
            _piano = value
        End Set
    End Property
    Private _piano As String
	
	Public Property scala() As String
        Get
            Return _scala
        End Get
        Set(value As String)
            _scala = value
        End Set
    End Property
    Private _scala As String
	
	Public Property interno() As String
        Get
            Return _interno
        End Get
        Set(value As String)
            _interno = value
        End Set
    End Property
    Private _interno As String

    Public Property livelli() As String
        Get
            Return _livelli
        End Get
        Set(value As String)
            _livelli = value
        End Set
    End Property
    Private _livelli As String

    Public Property locali() As String
        Get
            Return _locali
        End Get
        Set(value As String)
            _locali = value
        End Set
    End Property
    Private _locali As String

    Public Property camere() As String
        Get
            Return _camere
        End Get
        Set(value As String)
            _camere = value
        End Set
    End Property
    Private _camere As String

    Public Property bagni() As String
        Get
            Return _bagni
        End Get
        Set(value As String)
            _bagni = value
        End Set
    End Property
    Private _bagni As String

    Public Property tipo_cucina() As String
        Get
            Return _tipo_cucina
        End Get
        Set(value As String)
            _tipo_cucina = value
        End Set
    End Property
    Private _tipo_cucina As String

    Public Property pertinenze() As String
        Get
            Return _pertinenze
        End Get
        Set(value As String)
            _pertinenze = value
        End Set
    End Property
    Private _pertinenze As String

    Public Property impianti() As String
        Get
            Return _impianti
        End Get
        Set(value As String)
            _impianti = value
        End Set
    End Property
    Private _impianti As String

    Public Property pavimenti() As String
        Get
            Return _pavimenti
        End Get
        Set(value As String)
            _pavimenti = value
        End Set
    End Property
    Private _pavimenti As String

    Public Property app_pavimenti() As String
        Get
            Return _app_pavimenti
        End Get
        Set(value As String)
            _app_pavimenti = value
        End Set
    End Property
    Private _app_pavimenti As String

    Public Property app_riscaldamento() As String
        Get
            Return _app_riscaldamento
        End Get
        Set(value As String)
            _app_riscaldamento = value
        End Set
    End Property
    Private _app_riscaldamento As String

    Public Property app_impianti() As String
        Get
            Return _app_impianti
        End Get
        Set(value As String)
            _app_impianti = value
        End Set
    End Property
    Private _app_impianti As String

    Public Property app_infissi() As String
        Get
            Return _app_infissi
        End Get
        Set(value As String)
            _app_infissi = value
        End Set
    End Property
    Private _app_infissi As String

    Public Property rifiniture() As String
        Get
            Return _rifiniture
        End Get
        Set(value As String)
            _rifiniture = value
        End Set
    End Property
    Private _rifiniture As String

    Public Property servizi() As String
        Get
            Return _servizi
        End Get
        Set(value As String)
            _servizi = value
        End Set
    End Property
    Private _servizi As String

    Public Property valore() As String
        Get
            Return _valore
        End Get
        Set(value As String)
            _valore = value
        End Set
    End Property
    Private _valore As String

    Public Property spese_condominio() As String
        Get
            Return _spese_condominio
        End Get
        Set(value As String)
            _spese_condominio = value
        End Set
    End Property
    Private _spese_condominio As String

    Public Property spese_riscaldamento() As String
        Get
            Return _spese_riscaldamento
        End Get
        Set(value As String)
            _spese_riscaldamento = value
        End Set
    End Property
    Private _spese_riscaldamento As String
	
	Public Property id_appuntamento() As Integer
        Get
            Return _id_appuntamento
        End Get
        Set(value As Integer)
            _id_appuntamento = value
        End Set
    End Property
	Private _id_appuntamento As Integer
	
	Public Property data_primo_appuntamento() As String
        Get
            Return _data_primo_appuntamento
        End Get
        Set(value As String)
            _data_primo_appuntamento = value
        End Set
    End Property
    Private _data_primo_appuntamento As String	

    Public Property data_scadenza() As String
        Get
            Return _data_scadenza
        End Get
        Set(value As String)
            _data_scadenza = value
        End Set
    End Property
    Private _data_scadenza As String
	
	Public Property data_termine() As String
        Get
            Return _data_termine
        End Get
        Set(value As String)
            _data_termine = value
        End Set
    End Property
    Private _data_termine As String
	
	Public Property data_verifica() As String
        Get
            Return _data_verifica
        End Get
        Set(value As String)
            _data_verifica = value
        End Set
    End Property
    Private _data_verifica As String

    Public Property mutuo() As String
        Get
            Return _mutuo
        End Get
        Set(value As String)
            _mutuo = value
        End Set
    End Property
    Private _mutuo As String

    Public Property margine() As String
        Get
            Return _margine
        End Get
        Set(value As String)
            _margine = value
        End Set
    End Property
    Private _margine As String

    Public Property margineMq() As String
        Get
            Return _margineMq
        End Get
        Set(value As String)
            _margineMq = value
        End Set
    End Property
    Private _margineMq As String

    Public Property acconto() As String
        Get
            Return _acconto
        End Get
        Set(value As String)
            _acconto = value
        End Set
    End Property
    Private _acconto As String

    Public Property documenti() As String
        Get
            Return _documenti
        End Get
        Set(value As String)
            _documenti = value
        End Set
    End Property
    Private _documenti As String

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String

    Public Property foto1() As String
        Get
            Return _foto1
        End Get
        Set(value As String)
            _foto1 = value
        End Set
    End Property
    Private _foto1 As String

    Public Property foto2() As String
        Get
            Return _foto2
        End Get
        Set(value As String)
            _foto2 = value
        End Set
    End Property
    Private _foto2 As String

    Public Property foto3() As String
        Get
            Return _foto3
        End Get
        Set(value As String)
            _foto3 = value
        End Set
    End Property
    Private _foto3 As String

    Public Property foto4() As String
        Get
            Return _foto4
        End Get
        Set(value As String)
            _foto4 = value
        End Set
    End Property
    Private _foto4 As String

    Public Property foto5() As String
        Get
            Return _foto5
        End Get
        Set(value As String)
            _foto5 = value
        End Set
    End Property
    Private _foto5 As String

    Public Property foto6() As String
        Get
            Return _foto6
        End Get
        Set(value As String)
            _foto6 = value
        End Set
    End Property
    Private _foto6 As String

    Public Property foto7() As String
        Get
            Return _foto7
        End Get
        Set(value As String)
            _foto7 = value
        End Set
    End Property
    Private _foto7 As String

    Public Property foto8() As String
        Get
            Return _foto8
        End Get
        Set(value As String)
            _foto8 = value
        End Set
    End Property
    Private _foto8 As String

    Public Property foto9() As String
        Get
            Return _foto9
        End Get
        Set(value As String)
            _foto9 = value
        End Set
    End Property
    Private _foto9 As String

    Public Property foto10() As String
        Get
            Return _foto10
        End Get
        Set(value As String)
            _foto10 = value
        End Set
    End Property
    Private _foto10 As String

    Public Property regione() As String
        Get
            Return _regione
        End Get
        Set(value As String)
            _regione = value
        End Set
    End Property
    Private _regione As String

    Public Property provincia() As String
        Get
            Return _provincia
        End Get
        Set(value As String)
            _provincia = value
        End Set
    End Property
    Private _provincia As String

    Public Property idprovincia() As String
        Get
            Return _idprovincia
        End Get
        Set(value As String)
            _idprovincia = value
        End Set
    End Property
    Private _idprovincia As String

    Public Property spese_extra() As String
        Get
            Return _spese_extra
        End Get
        Set(value As String)
            _spese_extra = value
        End Set
    End Property
    Private _spese_extra As String

    Public Property spese_sospeso() As String
        Get
            Return _spese_sospeso
        End Get
        Set(value As String)
            _spese_sospeso = value
        End Set
    End Property
    Private _spese_sospeso As String

    Public Property valore_mercato() As String
        Get
            Return _valore_mercato
        End Get
        Set(value As String)
            _valore_mercato = value
        End Set
    End Property
    Private _valore_mercato As String

    Public Property idcomune() As String
        Get
            Return _comune
        End Get
        Set(value As String)
            _comune = value
        End Set
    End Property
    Private _comune As String

    Public Property jolly1() As String
        Get
            Return _jolly1
        End Get
        Set(value As String)
            _jolly1 = value
        End Set
    End Property
    Private _jolly1 As String

    Public Property jolly2() As String
        Get
            Return _jolly2
        End Get
        Set(value As String)
            _jolly2 = value
        End Set
    End Property
    Private _jolly2 As String

    Public Property data_inserimento() As String
        Get
            Return _data_inserimento
        End Get
        Set(value As String)
            _data_inserimento = value
        End Set
    End Property
    Private _data_inserimento As String

    Public Property data_modifica() As String
        Get
            Return _data_modifica
        End Get
        Set(value As String)
            _data_modifica = value
        End Set
    End Property
    Private _data_modifica As String

    Public Property disponibilita() As String
        Get
            Return _disponibilita
        End Get
        Set(value As String)
            _disponibilita = value
        End Set
    End Property
    Private _disponibilita As String

    Public Property ascensore() As String
        Get
            Return _ascensore
        End Get
        Set(value As String)
            _ascensore = value
        End Set
    End Property
    Private _ascensore As String

	Public Property portiere() As String
        Get
            Return _portiere
        End Get
        Set(value As String)
            _portiere = value
        End Set
    End Property
    Private _portiere As String
	
    Public Property cantina() As String
        Get
            Return _cantina
        End Get
        Set(value As String)
            _cantina = value
        End Set
    End Property
    Private _cantina As String
	
	Public Property giardino() As String
        Get
            Return _giardino
        End Get
        Set(value As String)
            _giardino = value
        End Set
    End Property
    Private _giardino As String
	
	Public Property autobox() As String
        Get
            Return _autobox
        End Get
        Set(value As String)
            _autobox = value
        End Set
    End Property
    Private _autobox As String

    Public Property locali_annessi() As String
        Get
            Return _locali_annessi
        End Get
        Set(value As String)
            _locali_annessi = value
        End Set
    End Property
    Private _locali_annessi As String

	Public Property balconi() As String
        Get
            Return _balconi
        End Get
        Set(value As String)
            _balconi = value
        End Set
    End Property
    Private _balconi As String

    Public Property mq_balconi() As String
        Get
            Return _mq_balconi
        End Get
        Set(value As String)
            _mq_balconi = value
        End Set
    End Property
    Private _mq_balconi As String

    Public Property mq_areaverde() As String
        Get
            Return _mq_areaverde
        End Get
        Set(value As String)
            _mq_areaverde = value
        End Set
    End Property
    Private _mq_areaverde As String

	Public Property terrazzi() As String
        Get
            Return _terrazzi
        End Get
        Set(value As String)
            _terrazzi = value
        End Set
    End Property
    Private _terrazzi As String
	
    Public Property mq_terrazzi() As String
        Get
            Return _mq_terrazzi
        End Get
        Set(value As String)
            _mq_terrazzi = value
        End Set
    End Property
    Private _mq_terrazzi As String

    Public Property azioni() As String
        Get
            Return _azioni
        End Get
        Set(value As String)
            _azioni = value
        End Set
    End Property
    Private _azioni As String

    Public Property bloccato() As String
        Get
            Return _bloccato
        End Get
        Set(value As String)
            _bloccato = value
        End Set
    End Property
    Private _bloccato As String

    Public Property condominio() As String
        Get
            Return _condominio
        End Get
        Set(value As String)
            _condominio = value
        End Set
    End Property
    Private _condominio As String

    Public Property RenditaCatastale() As String
        Get
            Return _RenditaCatastale
        End Get
        Set(value As String)
            _RenditaCatastale = value
        End Set
    End Property
    Private _RenditaCatastale As String

    Public Property tipoincarico() As String
        Get
            Return _tipoincarico
        End Get
        Set(value As String)
            _tipoincarico = value
        End Set
    End Property
    Private _tipoincarico As String

    Public Property riscaldamento() As String
        Get
            Return _riscaldamento
        End Get
        Set(value As String)
            _riscaldamento = value
        End Set
    End Property
    Private _riscaldamento As String

    Public Property descrizione_interna() As String
        Get
            Return _descrizione_interna
        End Get
        Set(value As String)
            _descrizione_interna = value
        End Set
    End Property
    Private _descrizione_interna As String

    Public Property descrizione_breve() As String
        Get
            Return _descrizione_breve
        End Get
        Set(value As String)
            _descrizione_breve = value
        End Set
    End Property
    Private _descrizione_breve As String

    Public Property catasto() As String
        Get
            Return _catasto
        End Get
        Set(value As String)
            _catasto = value
        End Set
    End Property
    Private _catasto As String
	
	Public Property rapporto() As String
        Get
            Return _rapporto
        End Get
        Set(value As String)
            _rapporto = value
        End Set
    End Property
    Private _rapporto As String

    Public Property valore_Stima() As String
        Get
            Return _valore_Stima
        End Get
        Set(value As String)
            _valore_Stima = value
        End Set
    End Property
    Private _valore_Stima As String
	
	Public Property esito() As String
        Get
            Return _esito
        End Get
        Set(value As String)
            _esito = value
        End Set
    End Property
    Private _esito As String
	
	Public Property status() As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property
    Private _status As String
	
	Public Property note() As String
        Get
            Return _note
        End Get
        Set(value As String)
            _note = value
        End Set
    End Property
    Private _note As String
	
	Public Property registro() As String
        Get
            Return _registro
        End Get
        Set(value As String)
            _registro = value
        End Set
    End Property
    Private _registro As String
	
	Public Property ipe() As String
        Get
            Return _ipe
        End Get
        Set(value As String)
            _ipe = value
        End Set
    End Property
    Private _ipe As String
	
	Public Property idconsulente() As String
        Get
            Return _idconsulente
        End Get
        Set(value As String)
            _idconsulente = value
        End Set
    End Property
    Private _idconsulente As String

	
	Public Property idacquisitore() As String
        Get
            Return _idacquisitore
        End Get
        Set(value As String)
            _idacquisitore = value
        End Set
    End Property
    Private _idacquisitore As String
	
	Public Property modPag() As String
        Get
            Return _modPag
        End Get
        Set(value As String)
            _modPag = value
        End Set
    End Property
    Private _modPag As String
	
	Public Property cliente_nominativo As String
        Get
            Return _cliente_nominativo
        End Get
        Set(value As String)
            _cliente_nominativo = value
        End Set
    End Property
    Private _cliente_nominativo As String
	
    Public Property cliente_tipoContatto As String
        Get
            Return _cliente_tipoContatto
        End Get
        Set(value As String)
            _cliente_tipoContatto = value
        End Set
    End Property
    Private _cliente_tipoContatto As String
				
	Public Property cliente_note As String
        Get
            Return _cliente_note
        End Get
        Set(value As String)
            _cliente_note = value
        End Set
    End Property
    Private _cliente_note As String
				
	Public Property cliente_telefono As String
        Get
            Return _cliente_telefono
        End Get
        Set(value As String)
            _cliente_telefono = value
        End Set
    End Property
    Private _cliente_telefono As String
				
	Public Property cliente_cellulare As String
        Get
            Return _cliente_cellulare
        End Get
        Set(value As String)
            _cliente_cellulare = value
        End Set
    End Property
    Private _cliente_cellulare As String
				
	Public Property cliente_email As String
        Get
            Return _cliente_email
        End Get
        Set(value As String)
            _cliente_email = value
        End Set
    End Property
    Private _cliente_email As String
				
	Public Property cliente_cap As String
        Get
            Return _cliente_cap
        End Get
        Set(value As String)
            _cliente_cap = value
        End Set
    End Property
    Private _cliente_cap As String
				
	Public Property cliente_indirizzo As String
        Get
            Return _cliente_indirizzo
        End Get
        Set(value As String)
            _cliente_indirizzo = value
        End Set
    End Property
    Private _cliente_indirizzo As String
				
	Public Property cliente_citta As String
        Get
            Return _cliente_citta
        End Get
        Set(value As String)
            _cliente_citta = value
        End Set
    End Property
    Private _cliente_citta As String
				
	Public Property cliente_delegato As String
        Get
            Return _cliente_delegato
        End Get
        Set(value As String)
            _cliente_delegato = value
        End Set
    End Property
    Private _cliente_delegato As String
				
	Public Property cliente_delegato_telefono As String
        Get
            Return _cliente_delegato_telefono
        End Get
        Set(value As String)
            _cliente_delegato_telefono = value
        End Set
    End Property
    Private _cliente_delegato_telefono As String
	
	Public Property cliente_delegato_cellulare As String
        Get
            Return _cliente_delegato_cellulare
        End Get
        Set(value As String)
            _cliente_delegato_cellulare = value
        End Set
    End Property
    Private _cliente_delegato_cellulare As String
	
	Public Property cliente_delegato_email As String
        Get
            Return _cliente_delegato_email
        End Get
        Set(value As String)
            _cliente_delegato_email = value
        End Set
    End Property
    Private _cliente_delegato_email As String

	Public Property longitudine As String
        Get
            Return _longitudine
        End Get
        Set(value As String)
            _longitudine = value
        End Set
    End Property
    Private _longitudine As String

	Public Property latitudine As String
        Get
            Return _latitudine
        End Get
        Set(value As String)
            _latitudine = value
        End Set
    End Property
    Private _latitudine As String

	
	
	Public Property area2 As String
        Get
            Return _area2
        End Get
        Set(value As String)
            _area2 = value
        End Set
    End Property
	Private _area2 					As String
	
	Public Property area3 As String
        Get
            Return _area3
        End Get
        Set(value As String)
            _area3 = value
        End Set
    End Property
	Private _area3 				    As String
	
	Public Property tipologia2 As String
        Get
            Return _tipologia2
        End Get
        Set(value As String)
            _tipologia2 = value
        End Set
    End Property
	Private _tipologia2 			As String
	
	Public Property tipologia3 As String
        Get
            Return _tipologia3
        End Get
        Set(value As String)
            _tipologia3 = value
        End Set
    End Property
	Private _tipologia3 			As String
	
	Public Property palazzina As String
        Get
            Return _palazzina
        End Get
        Set(value As String)
            _palazzina = value
        End Set
    End Property
	Private _palazzina 			    As String
	
	Public Property facciata As String
        Get
            Return _facciata
        End Get
        Set(value As String)
            _facciata = value
        End Set
    End Property
	Private _facciata 				As String
	
	Public Property app_tipoAnnullameto As String
        Get
            Return _app_tipoAnnullameto
        End Get
        Set(value As String)
            _app_tipoAnnullameto = value
        End Set
    End Property
	Private _app_tipoAnnullameto 	As String
	
	Public Property app_esitoPositivo As String
        Get
            Return _app_esitoPositivo
        End Get
        Set(value As String)
            _app_esitoPositivo = value
        End Set
    End Property
	Private _app_esitoPositivo 	    As String
	
	Public Property app_esitoNegativo As String
        Get
            Return _app_esitoNegativo
        End Get
        Set(value As String)
            _app_esitoNegativo = value
        End Set
    End Property
	Private _app_esitoNegativo 	    As String
	
	Public Property pratica_tipologia As String
        Get
            Return _pratica_tipologia
        End Get
        Set(value As String)
            _pratica_tipologia = value
        End Set
    End Property
	Private _pratica_tipologia 	    As String
	
	Public Property locali2 As String
        Get
            Return _locali2
        End Get
        Set(value As String)
            _locali2 = value
        End Set
    End Property
	Private _locali2 				As String
	
	Public Property mutuo_cliente As String
        Get
            Return _mutuo_cliente
        End Get
        Set(value As String)
            _mutuo_cliente = value
        End Set
    End Property
	Private _mutuo_cliente 		    As String
	
	Public Property marketing_cantiere As String
        Get
            Return _marketing_cantiere
        End Get
        Set(value As String)
            _marketing_cantiere = value
        End Set
    End Property
	Private _marketing_cantiere 	As String
	
	Public Property marketing_attivita As String
        Get
            Return _marketing_attivita
        End Get
        Set(value As String)
            _marketing_attivita = value
        End Set
    End Property
	Private _marketing_attivita 	As String
	
	Public Property marketing_quartiere As String
        Get
            Return _marketing_quartiere
        End Get
        Set(value As String)
            _marketing_quartiere = value
        End Set
    End Property
	Private _marketing_quartiere 	As String
	
	Public Property locali3 As String
        Get
            Return _locali3
        End Get
        Set(value As String)
            _locali3 = value
        End Set
    End Property
	Private _locali3 				As String
	
	Public Property delegato As String
        Get
            Return _delegato
        End Get
        Set(value As String)
            _delegato = value
        End Set
    End Property
	Private _delegato 				As String
	
	Public Property fontePubblicitaria() As String
        Get
            Return _fontePubblicitaria
        End Get
        Set(value As String)
            _fontePubblicitaria = value
        End Set
    End Property
    Private _fontePubblicitaria As String
	
End Class
