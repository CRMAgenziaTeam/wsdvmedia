﻿Public Class Jolly

    Public Property valore() As String
        Get
            Return _valore
        End Get
        Set(value As String)
            _valore = value
        End Set
    End Property
    Private _valore As String

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String

End Class
