﻿Imports Microsoft.VisualBasic

Public Class Setup

	Public Property iddvm() As String
        Get
            Return _iddvm
        End Get
        Set(value As String)
            _iddvm = value
        End Set
    End Property
    Private _iddvm As String

    Public Property colore() As String
        Get
            Return _colore
        End Get
        Set(value As String)
            _colore = value
        End Set
    End Property
    Private _colore As String

    Public Property UrlFoto() As String
        Get
            Return _UrlFoto
        End Get
        Set(value As String)
            _UrlFoto = value
        End Set
    End Property
    Private _UrlFoto As String

    Public Property telefono2() As String
        Get
            Return _telefono2
        End Get
        Set(value As String)
            _telefono2 = value
        End Set
    End Property
    Private _telefono2 As String

    Public Property Jolly1() As String
        Get
            Return _Jolly1
        End Get
        Set(value As String)
            _Jolly1 = value
        End Set
    End Property
    Private _Jolly1 As String

    Public Property Jolly2() As String
        Get
            Return _Jolly2
        End Get
        Set(value As String)
            _Jolly2 = value
        End Set
    End Property
    Private _Jolly2 As String

    Public Property idanagrafica() As String
        Get
            Return _idanagrafica
        End Get
        Set(value As String)
            _idanagrafica = value
        End Set
    End Property
    Private _idanagrafica As String
	
	Public Property idUtente() As String
        Get
            Return _idUtente
        End Get
        Set(value As String)
            _idUtente = value
        End Set
    End Property
    Private _idUtente As String

    Public Property Nominativo() As String
        Get
            Return _Nominativo
        End Get
        Set(value As String)
            _Nominativo = value
        End Set
    End Property
    Private _Nominativo As String

    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(value As String)
            _Email = value
        End Set
    End Property
    Private _Email As String

    Public Property TipoUtente() As String
        Get
            Return _TipoUtente
        End Get
        Set(value As String)
            _TipoUtente = value
        End Set
    End Property
    Private _TipoUtente As String

    Public Property Condivisione() As String
        Get
            Return _Condivisione
        End Get
        Set(value As String)
            _Condivisione = value
        End Set
    End Property
    Private _Condivisione As String

    Public Property Attivo() As String
        Get
            Return _Attivo
        End Get
        Set(value As String)
            _Attivo = value
        End Set
    End Property
    Private _Attivo As String

    Public Property Privacy() As String
        Get
            Return _Privacy
        End Get
        Set(value As String)
            _Privacy = value
        End Set
    End Property
    Private _Privacy As String

    Public Property link_Privacy() As String
        Get
            Return _link_Privacy
        End Get
        Set(value As String)
            _link_Privacy = value
        End Set
    End Property
    Private _link_Privacy As String

    Public Property EmailGCalendar() As String
        Get
            Return _EmailGCalendar
        End Get
        Set(value As String)
            _EmailGCalendar = value
        End Set
    End Property
    Private _EmailGCalendar As String

    Public Property PasswordGCalendar() As String
        Get
            Return _PasswordGCalendar
        End Get
        Set(value As String)
            _PasswordGCalendar = value
        End Set
    End Property
    Private _PasswordGCalendar As String

    Public Property HostFTP() As String
        Get
            Return _HostFTP
        End Get
        Set(value As String)
            _HostFTP = value
        End Set
    End Property
    Private _HostFTP As String

    Public Property UserFTP() As String
        Get
            Return _UserFTP
        End Get
        Set(value As String)
            _UserFTP = value
        End Set
    End Property
    Private _UserFTP As String

    Public Property PasswordFTP() As String
        Get
            Return _PasswordFTP
        End Get
        Set(value As String)
            _PasswordFTP = value
        End Set
    End Property
    Private _PasswordFTP As String

    Public Property Scheda_Incarichi() As String
        Get
            Return _Tot_Incarichi
        End Get
        Set(value As String)
            _Tot_Incarichi = value
        End Set
    End Property
    Private _Tot_Incarichi As String

    Public Property Scheda_Richieste() As String
        Get
            Return _Tot_Richieste
        End Get
        Set(value As String)
            _Tot_Richieste = value
        End Set
    End Property
    Private _Tot_Richieste As String

    Public Property Banner_small() As String
        Get
            Return _Banner_small
        End Get
        Set(value As String)
            _Banner_small = value
        End Set
    End Property
    Private _Banner_small As String

    Public Property Banner_big() As String
        Get
            Return _Banner_big
        End Get
        Set(value As String)
            _Banner_big = value
        End Set
    End Property
    Private _Banner_big As String

    Public Property link_Banner() As String
        Get
            Return _link_Banner
        End Get
        Set(value As String)
            _link_Banner = value
        End Set
    End Property
    Private _link_Banner As String

    Public Property idcomune() As String
        Get
            Return _comune
        End Get
        Set(value As String)
            _comune = value
        End Set
    End Property
    Private _comune As String

    Public Property idprovincia() As String
        Get
            Return _provincia
        End Get
        Set(value As String)
            _provincia = value
        End Set
    End Property
    Private _provincia As String

    Public Property idregione() As String
        Get
            Return _regione
        End Get
        Set(value As String)
            _regione = value
        End Set
    End Property
    Private _regione As String

    Public Property versione() As String
        Get
            Return _versione
        End Get
        Set(value As String)
            _versione = value
        End Set
    End Property
    Private _versione As String

    Public Property root_Foto() As String
        Get
            Return _root_Foto
        End Get
        Set(value As String)
            _root_Foto = value
        End Set
    End Property
    Private _root_Foto As String

    Public Property Nome_APP() As String
        Get
            Return _Nome_APP
        End Get
        Set(value As String)
            _Nome_APP = value
        End Set
    End Property
    Private _Nome_APP As String

    Public Property Email_assistenza() As String
        Get
            Return _Email_assistenza
        End Get
        Set(value As String)
            _Email_assistenza = value
        End Set
    End Property
    Private _Email_assistenza As String

    Public Property Tel_Agenzia() As String
        Get
            Return _Tel_Agenzia
        End Get
        Set(value As String)
            _Tel_Agenzia = value
        End Set
    End Property
    Private _Tel_Agenzia As String

    Public Property latitudine() As String
        Get
            Return _latitudine
        End Get
        Set(value As String)
            _latitudine = value
        End Set
    End Property
    Private _latitudine As String

    Public Property longitudine() As String
        Get
            Return _longitudine
        End Get
        Set(value As String)
            _longitudine = value
        End Set
    End Property
    Private _longitudine As String

    Public Property classifica() As String
        Get
            Return _classifica
        End Get
        Set(value As String)
            _classifica = value
        End Set
    End Property
    Private _classifica As String

    Public Property filiale() As String
        Get
            Return _filiale
        End Get
        Set(value As String)
            _filiale = value
        End Set
    End Property
    Private _filiale As String
	
	Public Property delegato() As String
        Get
            Return _delegato
        End Get
        Set(value As String)
            _delegato = value
        End Set
    End Property
    Private _delegato As String
	
	Public Property gruppo() As String
        Get
            Return _gruppo
        End Get
        Set(value As String)
            _gruppo = value
        End Set
    End Property
    Private _gruppo As String

	Public Property FotoStandardMax() As String
        Get
            Return _FotoStandardMax
        End Get
        Set(value As String)
            _FotoStandardMax = value
        End Set
    End Property
    Private _FotoStandardMax As String
	
	Public Property FotoFullMax() As String
        Get
            Return _FotoFullMax
        End Get
        Set(value As String)
            _FotoFullMax = value
        End Set
    End Property
    Private _FotoFullMax As String

	
	Public Property url_social1() As String
		Get
            Return _url_social1
        End Get
        Set(value As String)
            _url_social1 = value
        End Set
    End Property
    Private _url_social1 As String
	
	Public Property url_social2() As String
		Get
            Return _url_social2
        End Get
        Set(value As String)
            _url_social2 = value
        End Set
    End Property
    Private _url_social2 As String
	
	Public Property url_social3() As String
		Get
            Return _url_social3
        End Get
        Set(value As String)
            _url_social3 = value
        End Set
    End Property
    Private _url_social3 As String
	
	Public Property descrizione() As String
		Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String
	
	Public Property slogan() As String
		Get
            Return _slogan
        End Get
        Set(value As String)
            _slogan = value
        End Set
    End Property
    Private _slogan As String
	
	Public Property url_sitoweb() As String
		Get
            Return _url_sitoweb
        End Get
        Set(value As String)
            _url_sitoweb = value
        End Set
    End Property
    Private _url_sitoweb As String
	
	Public Property url_privacy() As String
		Get
            Return _url_privacy
        End Get
        Set(value As String)
            _url_privacy = value
        End Set
    End Property
    Private _url_privacy As String
	
	Public Property nominativoAgenzia() As String
		Get
            Return _nominativoAgenzia
        End Get
        Set(value As String)
            _nominativoAgenzia = value
        End Set
    End Property
    Private _nominativoAgenzia As String
	
	Public Property telefono_1() As String
		Get
            Return _telefono_1
        End Get
        Set(value As String)
            _telefono_1 = value
        End Set
    End Property
    Private _telefono_1 As String
	
	Public Property ind_nominativo() As String
		Get
            Return _ind_nominativo
        End Get
        Set(value As String)
            _ind_nominativo = value
        End Set
    End Property
    Private _ind_nominativo As String
	
	Public Property cap_nom() As String
		Get
            Return _cap_nom
        End Get
        Set(value As String)
            _cap_nom = value
        End Set
    End Property
    Private _cap_nom As String
	
	Public Property telefono_2() As String
		Get
            Return _telefono_2
        End Get
        Set(value As String)
            _telefono_2 = value
        End Set
    End Property
    Private _telefono_2 As String
	
	Public Property PartIva() As String
		Get
            Return _PartIva
        End Get
        Set(value As String)
            _PartIva = value
        End Set
    End Property
    Private _PartIva As String
	
	Public Property email_agenzia() As String
		Get
            Return _email_agenzia
        End Get
        Set(value As String)
            _email_agenzia = value
        End Set
    End Property
    Private _email_agenzia As String
	
	
End Class
