﻿Public Class Zona

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property
    Private _id As String

    Public Property denominazione() As String
        Get
            Return _denominazione
        End Get
        Set(value As String)
            _denominazione = value
        End Set
    End Property
    Private _denominazione As String

    Public Property comune() As String
        Get
            Return _comune
        End Get
        Set(value As String)
            _comune = value
        End Set
    End Property
    Private _comune As String

    Public Property provincia() As String
        Get
            Return _provincia
        End Get
        Set(value As String)
            _provincia = value
        End Set
    End Property
    Private _provincia As String

    Public Property regione() As String
        Get
            Return _regione
        End Get
        Set(value As String)
            _regione = value
        End Set
    End Property
    Private _regione As String

	Public Property cap() As String
        Get
            Return _cap
        End Get
        Set(value As String)
            _cap = value
        End Set
    End Property
    Private _cap As String

End Class