﻿Imports Microsoft.VisualBasic

Public Class scheda

    Public Property filiale() As String
        Get
            Return _filiale
        End Get
        Set(value As String)
            _filiale = value
        End Set
    End Property
    Private _filiale As String

    Public Property priorita() As String
        Get
            Return _priorita
        End Get
        Set(value As String)
            _priorita = value
        End Set
    End Property
    Private _priorita As String

    Public Property vetrina() As String
        Get
            Return _vetrina
        End Get
        Set(value As String)
            _vetrina = value
        End Set
    End Property
    Private _vetrina As String

    Public Property data_inserimento() As String
        Get
            Return _data_inserimento
        End Get
        Set(value As String)
            _data_inserimento = value
        End Set
    End Property
    Private _data_inserimento As String

    Public Property data_modifica() As String
        Get
            Return _data_modifica
        End Get
        Set(value As String)
            _data_modifica = value
        End Set
    End Property
    Private _data_modifica As String

    Public Property riservato() As String
        Get
            Return _riservato
        End Get
        Set(value As String)
            _riservato = value
        End Set
    End Property
    Private _riservato As String

    Public Property area() As String
        Get
            Return _area
        End Get
        Set(value As String)
            _area = value
        End Set
    End Property
    Private _area As String

    Public Property codice() As String
        Get
            Return _codice
        End Get
        Set(value As String)
            _codice = value
        End Set
    End Property
    Private _codice As String

    Public Property tipologia() As String
        Get
            Return _tipologia
        End Get
        Set(value As String)
            _tipologia = value
        End Set
    End Property
    Private _tipologia As String

    Public Property prezzo() As String
        Get
            Return _prezzo
        End Get
        Set(value As String)
            _prezzo = value
        End Set
    End Property
    Private _prezzo As String

    Public Property mq() As String
        Get
            Return _mq
        End Get
        Set(value As String)
            _mq = value
        End Set
    End Property
    Private _mq As String

    Public Property camere() As String
        Get
            Return _camere
        End Get
        Set(value As String)
            _camere = value
        End Set
    End Property
    Private _camere As String

    Public Property locali() As String
        Get
            Return _locali
        End Get
        Set(value As String)
            _locali = value
        End Set
    End Property
    Private _locali As String

    Public Property latitudine() As String
        Get
            Return _latitudine
        End Get
        Set(value As String)
            _latitudine = value
        End Set
    End Property
    Private _latitudine As String

    Public Property longitudine() As String
        Get
            Return _longitudine
        End Get
        Set(value As String)
            _longitudine = value
        End Set
    End Property
    Private _longitudine As String
	
	Public Property agenzia_latitudine() As String
        Get
            Return _agenzia_latitudine
        End Get
        Set(value As String)
            _agenzia_latitudine = value
        End Set
    End Property
    Private _agenzia_latitudine As String

    Public Property agenzia_longitudine() As String
        Get
            Return _agenzia_longitudine
        End Get
        Set(value As String)
            _agenzia_longitudine = value
        End Set
    End Property
    Private _agenzia_longitudine As String

    Public Property telefono_referente() As String
        Get
            Return _telefono_referente
        End Get
        Set(value As String)
            _telefono_referente = value
        End Set
    End Property
    Private _telefono_referente As String

    Public Property indirizzo_referente() As String
        Get
            Return _indirizzo_referente
        End Get
        Set(value As String)
            _indirizzo_referente = value
        End Set
    End Property
    Private _indirizzo_referente As String

    Public Property email_referente() As String
        Get
            Return _email_referente
        End Get
        Set(value As String)
            _email_referente = value
        End Set
    End Property
    Private _email_referente As String

    Public Property cell_referente() As String
        Get
            Return _cell_referente
        End Get
        Set(value As String)
            _cell_referente = value
        End Set
    End Property
    Private _cell_referente As String

    Public Property sito_referente() As String
        Get
            Return _sito_referente
        End Get
        Set(value As String)
            _sito_referente = value
        End Set
    End Property
    Private _sito_referente As String

    Public Property bagni() As String
        Get
            Return _bagni
        End Get
        Set(value As String)
            _bagni = value
        End Set
    End Property
    Private _bagni As String

    Public Property posto_auto() As String
        Get
            Return _Posto_auto
        End Get
        Set(value As String)
            _Posto_auto = value
        End Set
    End Property
    Private _Posto_auto As String

	Public Property idzona() As String
        Get
            Return _id_zona
        End Get
        Set(value As String)
            _id_zona = value
        End Set
    End Property
    Private _id_zona As String
	
    Public Property zona() As String
        Get
            Return _zona
        End Get
        Set(value As String)
            _zona = value
        End Set
    End Property
    Private _zona As String

    Public Property classe_energetica() As String
        Get
            Return _classe_energetica
        End Get
        Set(value As String)
            _classe_energetica = value
        End Set
    End Property
    Private _classe_energetica As String

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String

    'Public Property info() As String
    '    Get
    '        Return _info
    '    End Get
    '    Set(value As String)
    '        _info = value
    '    End Set
    'End Property
    'Private _info As String

    'Public Property info_completamento() As String
    '    Get
    '        Return _info_completamento
    '    End Get
    '    Set(value As String)
    '        _info_completamento = value
    '    End Set
    'End Property
    'Private _info_completamento As String

    Public Property pertinenze() As String
        Get
            Return _pertinenze
        End Get
        Set(value As String)
            _pertinenze = value
        End Set
    End Property
    Private _pertinenze As String

    Public Property impianti() As String
        Get
            Return _impianti
        End Get
        Set(value As String)
            _impianti = value
        End Set
    End Property
    Private _impianti As String

    Public Property pavimenti() As String
        Get
            Return _pavimenti
        End Get
        Set(value As String)
            _pavimenti = value
        End Set
    End Property
    Private _pavimenti As String

    Public Property accessori() As String
        Get
            Return _accessori
        End Get
        Set(value As String)
            _accessori = value
        End Set
    End Property
    Private _accessori As String

    Public Property servizi() As String
        Get
            Return _servizi
        End Get
        Set(value As String)
            _servizi = value
        End Set
    End Property
    Private _servizi As String

    Public Property foto1() As String
        Get
            Return _foto1
        End Get
        Set(value As String)
            _foto1 = value
        End Set
    End Property
    Private _foto1 As String

    Public Property foto2() As String
        Get
            Return _foto2
        End Get
        Set(value As String)
            _foto2 = value
        End Set
    End Property
    Private _foto2 As String

    Public Property foto3() As String
        Get
            Return _foto3
        End Get
        Set(value As String)
            _foto3 = value
        End Set
    End Property
    Private _foto3 As String

    Public Property foto4() As String
        Get
            Return _foto4
        End Get
        Set(value As String)
            _foto4 = value
        End Set
    End Property
    Private _foto4 As String

    Public Property foto5() As String
        Get
            Return _foto5
        End Get
        Set(value As String)
            _foto5 = value
        End Set
    End Property
    Private _foto5 As String

    Public Property foto6() As String
        Get
            Return _foto6
        End Get
        Set(value As String)
            _foto6 = value
        End Set
    End Property
    Private _foto6 As String

    Public Property foto7() As String
        Get
            Return _foto7
        End Get
        Set(value As String)
            _foto7 = value
        End Set
    End Property
    Private _foto7 As String

    Public Property foto8() As String
        Get
            Return _foto8
        End Get
        Set(value As String)
            _foto8 = value
        End Set
    End Property
    Private _foto8 As String

    Public Property foto9() As String
        Get
            Return _foto9
        End Get
        Set(value As String)
            _foto9 = value
        End Set
    End Property
    Private _foto9 As String

    Public Property foto10() As String
        Get
            Return _foto10
        End Get
        Set(value As String)
            _foto10 = value
        End Set
    End Property
    Private _foto10 As String

    Public Property foto11() As String
        Get
            Return _foto11
        End Get
        Set(value As String)
            _foto11 = value
        End Set
    End Property
    Private _foto11 As String

    Public Property foto12() As String
        Get
            Return _foto12
        End Get
        Set(value As String)
            _foto12 = value
        End Set
    End Property
    Private _foto12 As String

    Public Property foto13() As String
        Get
            Return _foto13
        End Get
        Set(value As String)
            _foto13 = value
        End Set
    End Property
    Private _foto13 As String

    Public Property foto14() As String
        Get
            Return _foto14
        End Get
        Set(value As String)
            _foto14 = value
        End Set
    End Property
    Private _foto14 As String

    Public Property foto15() As String
        Get
            Return _foto15
        End Get
        Set(value As String)
            _foto15 = value
        End Set
    End Property
    Private _foto15 As String

    Public Property foto16() As String
        Get
            Return _foto16
        End Get
        Set(value As String)
            _foto16 = value
        End Set
    End Property
    Private _foto16 As String

    Public Property foto17() As String
        Get
            Return _foto17
        End Get
        Set(value As String)
            _foto17 = value
        End Set
    End Property
    Private _foto17 As String

    Public Property foto18() As String
        Get
            Return _foto18
        End Get
        Set(value As String)
            _foto18 = value
        End Set
    End Property
    Private _foto18 As String

    Public Property foto19() As String
        Get
            Return _foto19
        End Get
        Set(value As String)
            _foto19 = value
        End Set
    End Property
    Private _foto19 As String

    Public Property foto20() As String
        Get
            Return _foto20
        End Get
        Set(value As String)
            _foto20 = value
        End Set
    End Property
    Private _foto20 As String

    Public Property affinity() As String
        Get
            Return _affinity
        End Get
        Set(value As String)
            _affinity = value
        End Set
    End Property
    Private _affinity As String

    Public Property indirizzo() As String
        Get
            Return _indirizzo
        End Get
        Set(value As String)
            _indirizzo = value
        End Set
    End Property
    Private _indirizzo As String

    Public Property urlVideo() As String
        Get
            Return _urlVideo
        End Get
        Set(value As String)
            _urlVideo = value
        End Set
    End Property
    Private _urlVideo As String

    Public Property urlVirtualTour() As String
        Get
            Return _urlVirtualTour
        End Get
        Set(value As String)
            _urlVirtualTour = value
        End Set
    End Property
    Private _urlVirtualTour As String
	
	Public Property urlLinkScheda() As String
        Get
            Return _urlLinkScheda
        End Get
        Set(value As String)
            _urlLinkScheda = value
        End Set
    End Property
    Private _urlLinkScheda As String


    Public Property urlFotoReferente() As String
        Get
            Return _urlFotoReferente
        End Get
        Set(value As String)
            _urlFotoReferente = value
        End Set
    End Property
    Private _urlFotoReferente As String

    Public Property urlLogoReferente() As String
        Get
            Return _urlLogoReferente
        End Get
        Set(value As String)
            _urlLogoReferente = value
        End Set
    End Property
    Private _urlLogoReferente As String


    Public Property comune() As String
        Get
            Return _comune
        End Get
        Set(value As String)
            _comune = value
        End Set
    End Property
    Private _comune As String

    Public Property idcomune() As String
        Get
            Return _idcomune
        End Get
        Set(value As String)
            _idcomune = value
        End Set
    End Property
    Private _idcomune As String

    Public Property spese_extra() As String
        Get
            Return _spese_extra
        End Get
        Set(value As String)
            _spese_extra = value
        End Set
    End Property
    Private _spese_extra As String

    Public Property spese_sospeso() As String
        Get
            Return _spese_sospeso
        End Get
        Set(value As String)
            _spese_sospeso = value
        End Set
    End Property
    Private _spese_sospeso As String

    Public Property valore_mercato() As String
        Get
            Return _valore_mercato
        End Get
        Set(value As String)
            _valore_mercato = value
        End Set
    End Property
    Private _valore_mercato As String

    Public Property idscheda() As String
        Get
            Return _id_scheda
        End Get
        Set(value As String)
            _id_scheda = value
        End Set
    End Property
    Private _id_scheda As String

    Public Property epoca() As String
        Get
            Return _epoca
        End Get
        Set(value As String)
            _epoca = value
        End Set
    End Property
    Private _epoca As String

    Public Property Livellistabile() As String
        Get
            Return _Livellistabile
        End Get
        Set(value As String)
            _Livellistabile = value
        End Set
    End Property
    Private _Livellistabile As String

    Public Property infoplus() As String
        Get
            Return _infoplus
        End Get
        Set(value As String)
            _infoplus = value
        End Set
    End Property
    Private _infoplus As String

    Public Property nome_referente() As String
        Get
            Return _nome_referente
        End Get
        Set(value As String)
            _nome_referente = value
        End Set
    End Property
    Private _nome_referente As String

    Public Property piano() As String
        Get
            Return _piano
        End Get
        Set(value As String)
            _piano = value
        End Set
    End Property
    Private _piano As String

    Public Property ipe() As String
        Get
            Return _ipe
        End Get
        Set(value As String)
            _ipe = value
        End Set
    End Property
    Private _ipe As String

    Public Property idanagrafica() As String
        Get
            Return _idanagrafica
        End Get
        Set(value As String)
            _idanagrafica = value
        End Set
    End Property
    Private _idanagrafica As String

    Public Property cell_scheda() As String
        Get
            Return _cell_scheda
        End Get
        Set(value As String)
            _cell_scheda = value
        End Set
    End Property
    Private _cell_scheda As String

    Public Property telefono_scheda() As String
        Get
            Return _telefono_scheda
        End Get
        Set(value As String)
            _telefono_scheda = value
        End Set
    End Property
    Private _telefono_scheda As String

    Public Property email_scheda() As String
        Get
            Return _email_scheda
        End Get
        Set(value As String)
            _email_scheda = value
        End Set
    End Property
    Private _email_scheda As String

    Public Property nome_scheda() As String
        Get
            Return _nome_scheda
        End Get
        Set(value As String)
            _nome_scheda = value
        End Set
    End Property
    Private _nome_scheda As String

    Public Property terrazzi() As String
        Get
            Return _terrazzi
        End Get
        Set(value As String)
            _terrazzi = value
        End Set
    End Property
    Private _terrazzi As String

    Public Property balconi() As String
        Get
            Return _balconi
        End Get
        Set(value As String)
            _balconi = value
        End Set
    End Property
    Private _balconi As String

    Public Property areaverde() As String
        Get
            Return _areaverde
        End Get
        Set(value As String)
            _areaverde = value
        End Set
    End Property
    Private _areaverde As String

    Public Property mq_areaverde() As String
        Get
            Return _mq_areaverde
        End Get
        Set(value As String)
            _mq_areaverde = value
        End Set
    End Property
    Private _mq_areaverde As String

    Public Property mq_terrazzi() As String
        Get
            Return _mq_terrazzi
        End Get
        Set(value As String)
            _mq_terrazzi = value
        End Set
    End Property
    Private _mq_terrazzi As String

    Public Property mq_balconi() As String
        Get
            Return _mq_balconi
        End Get
        Set(value As String)
            _mq_balconi = value
        End Set
    End Property
    Private _mq_balconi As String

    Public Property tipologia_inglese() As String
        Get
            Return _tipologia_inglese
        End Get
        Set(value As String)
            _tipologia_inglese = value
        End Set
    End Property
    Private _tipologia_inglese As String

    Public Property descrizione_inglese() As String
        Get
            Return _descrizione_inglese
        End Get
        Set(value As String)
            _descrizione_inglese = value
        End Set
    End Property
    Private _descrizione_inglese As String

    Public Property planimetria() As String
        Get
            Return _planimetria
        End Get
        Set(value As String)
            _planimetria = value
        End Set
    End Property
    Private _planimetria As String

    Public Property planimetria2() As String
        Get
            Return _planimetria2
        End Get
        Set(value As String)
            _planimetria2 = value
        End Set
    End Property
    Private _planimetria2 As String

    Public Property planimetria3() As String
        Get
            Return _planimetria3
        End Get
        Set(value As String)
            _planimetria3 = value
        End Set
    End Property
    Private _planimetria3 As String
	
	Public Property planimetria4() As String
        Get
            Return _planimetria4
        End Get
        Set(value As String)
            _planimetria4 = value
        End Set
    End Property
    Private _planimetria4 As String

    Public Property disponibilita() As String
        Get
            Return _disponibilita
        End Get
        Set(value As String)
            _disponibilita = value
        End Set
    End Property
    Private _disponibilita As String

    Public Property ascensore() As String
        Get
            Return _ascensore
        End Get
        Set(value As String)
            _ascensore = value
        End Set
    End Property
    Private _ascensore As String

    Public Property cantina() As String
        Get
            Return _cantina
        End Get
        Set(value As String)
            _cantina = value
        End Set
    End Property
    Private _cantina As String

    Public Property manutenzione() As String
        Get
            Return _manutenzione
        End Get
        Set(value As String)
            _manutenzione = value
        End Set
    End Property
    Private _manutenzione As String

    Public Property locali_annessi() As String
        Get
            Return _locali_annessi
        End Get
        Set(value As String)
            _locali_annessi = value
        End Set
    End Property
    Private _locali_annessi As String

    Public Property spese_condominio() As String
        Get
            Return _spese_condominio
        End Get
        Set(value As String)
            _spese_condominio = value
        End Set
    End Property
    Private _spese_condominio As String

    Public Property spese_riscaldamento() As String
        Get
            Return _spese_riscaldamento
        End Get
        Set(value As String)
            _spese_riscaldamento = value
        End Set
    End Property
    Private _spese_riscaldamento As String

    Public Property azioni() As String
        Get
            Return _azioni
        End Get
        Set(value As String)
            _azioni = value
        End Set
    End Property
    Private _azioni As String
	
	Public Property marketing() As String
        Get
            Return _marketing
        End Get
        Set(value As String)
            _marketing = value
        End Set
    End Property
    Private _marketing As String

    Public Property condominio() As String
        Get
            Return _condominio
        End Get
        Set(value As String)
            _condominio = value
        End Set
    End Property
    Private _condominio As String

    Public Property RenditaCatastale() As String
        Get
            Return _RenditaCatastale
        End Get
        Set(value As String)
            _RenditaCatastale = value
        End Set
    End Property
    Private _RenditaCatastale As String

    Public Property tipoincarico() As String
        Get
            Return _tipoincarico
        End Get
        Set(value As String)
            _tipoincarico = value
        End Set
    End Property
    Private _tipoincarico As String

    Public Property riscaldamento() As String
        Get
            Return _riscaldamento
        End Get
        Set(value As String)
            _riscaldamento = value
        End Set
    End Property
    Private _riscaldamento As String

    Public Property descrizione_interna() As String
        Get
            Return _descrizione_interna
        End Get
        Set(value As String)
            _descrizione_interna = value
        End Set
    End Property
    Private _descrizione_interna As String

    Public Property descrizione_breve() As String
        Get
            Return _descrizione_breve
        End Get
        Set(value As String)
            _descrizione_breve = value
        End Set
    End Property
    Private _descrizione_breve As String

    Public Property catasto() As String
        Get
            Return _catasto
        End Get
        Set(value As String)
            _catasto = value
        End Set
    End Property
    Private _catasto As String

    Public Property esito() As String
        Get
            Return _esito
        End Get
        Set(value As String)
            _esito = value
        End Set
    End Property
    Private _esito As String

    Public Property bloccato() As String
        Get
            Return _bloccato
        End Get
        Set(value As String)
            _bloccato = value
        End Set
    End Property
    Private _bloccato As String

    Public Property tipocucina() As String
        Get
            Return _tipocucina
        End Get
        Set(value As String)
            _tipocucina = value
        End Set
    End Property
    Private _tipocucina As String

    Public Property tipo() As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property
    Private _tipo As String

    Public Property note() As String
        Get
            Return _note
        End Get
        Set(value As String)
            _note = value
        End Set
    End Property
    Private _note As String

    Public Property scala() As String
        Get
            Return _scala
        End Get
        Set(value As String)
            _scala = value
        End Set
    End Property
    Private _scala As String

    Public Property interno() As String
        Get
            Return _interno
        End Get
        Set(value As String)
            _interno = value
        End Set
    End Property
    Private _interno As String

    Public Property esposizione() As String
        Get
            Return _esposizione
        End Get
        Set(value As String)
            _esposizione = value
        End Set
    End Property
    Private _esposizione As String

    Public Property data_verifica() As String
        Get
            Return _data_verifica
        End Get
        Set(value As String)
            _data_verifica = value
        End Set
    End Property
    Private _data_verifica As String
	
	Public Property data_scadenza() As String
        Get
            Return _data_scadenza
        End Get
        Set(value As String)
            _data_scadenza = value
        End Set
    End Property
    Private _data_scadenza As String

	Public Property idconsulente() As String
        Get
            Return _id_consulente
        End Get
        Set(value As String)
            _id_consulente = value
        End Set
    End Property
    Private _id_consulente As String
	
    Public Property consulente() As String
        Get
            Return _consulente
        End Get
        Set(value As String)
            _consulente = value
        End Set
    End Property
    Private _consulente As String

	Public Property idacquisitore() As String
        Get
            Return _id_acquisitore
        End Get
        Set(value As String)
            _id_acquisitore = value
        End Set
    End Property
    Private _id_acquisitore As String
	
    Public Property acquisitore() As String
        Get
            Return _acquisitore
        End Get
        Set(value As String)
            _acquisitore = value
        End Set
    End Property
    Private _acquisitore As String

    Public Property portiere() As String
        Get
            Return _portiere
        End Get
        Set(value As String)
            _portiere = value
        End Set
    End Property
    Private _portiere As String

    Public Property mq_calpestabili() As String
        Get
            Return _mq_calpestabili
        End Get
        Set(value As String)
            _mq_calpestabili = value
        End Set
    End Property
    Private _mq_calpestabili As String

    Public Property mq_localiannessi() As String
        Get
            Return _mq_localiannessi
        End Get
        Set(value As String)
            _mq_localiannessi = value
        End Set
    End Property
    Private _mq_localiannessi As String

    Public Property mq_coperti() As String
        Get
            Return _mq_coperti
        End Get
        Set(value As String)
            _mq_coperti = value
        End Set
    End Property
    Private _mq_coperti As String


    Public Property piani() As String
        Get
            Return _piani
        End Get
        Set(value As String)
            _piani = value
        End Set
    End Property
    Private _piani As String

    Public Property codice_agenzia() As String
        Get
            Return _codice_agenzia
        End Get
        Set(value As String)
            _codice_agenzia = value
        End Set
    End Property
    Private _codice_agenzia As String

    Public Property data_termine() As String
        Get
            Return _data_termine
        End Get
        Set(value As String)
            _data_termine = value
        End Set
    End Property
    Private _data_termine As String

    Public Property contratto_destinazione() As String
        Get
            Return _contratto_destinazione
        End Get
        Set(value As String)
            _contratto_destinazione = value
        End Set
    End Property
    Private _contratto_destinazione As String

    Public Property cap() As String
        Get
            Return _cap
        End Get
        Set(value As String)
            _cap = value
        End Set
    End Property
    Private _cap As String

    Public Property codiceOfferta() As String
        Get
            Return _codiceOfferta
        End Get
        Set(value As String)
            _codiceOfferta = value
        End Set
    End Property
    Private _codiceOfferta As String

    Public Property status() As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property
    Private _status As String
	
	Public Property fontePubblicitaria() As String
        Get
            Return _fontePubblicitaria
        End Get
        Set(value As String)
            _fontePubblicitaria = value
        End Set
    End Property
    Private _fontePubblicitaria As String

End Class
