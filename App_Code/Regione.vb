﻿Imports Microsoft.VisualBasic

Public Class regione

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property
    Private _id As String

    Public Property denominazione() As String
        Get
            Return _denominazione
        End Get
        Set(value As String)
            _denominazione = value
        End Set
    End Property
    Private _denominazione As String

End Class
