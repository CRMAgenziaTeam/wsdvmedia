﻿Imports Microsoft.VisualBasic

Public Class VisitaRemota

	Public Property id() As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property
    Private _id As String
	
	Public Property iddvm() As Integer
        Get
            Return _iddvm
        End Get
        Set(value As Integer)
            _iddvm = value
        End Set
    End Property
    Private _iddvm As Integer
	
	Public Property idBroker() As Integer
        Get
            Return _idBroker
        End Get
        Set(value As Integer)
            _idBroker = value
        End Set
    End Property
    Private _idBroker As Integer
	
	Public Property emailInvito() As String
        Get
            Return _emailInvito
        End Get
        Set(value As String)
            _emailInvito = value
        End Set
    End Property
    Private _emailInvito As String
	
	Public Property idScheda() As Integer
        Get
            Return _idScheda
        End Get
        Set(value As Integer)
            _idScheda = value
        End Set
    End Property
    Private _idScheda As Integer
	
	Public Property tipoRisorsaCondivisa() As Integer
        Get
            Return _tipoRisorsaCondivisa
        End Get
        Set(value As Integer)
            _tipoRisorsaCondivisa = value
        End Set
    End Property
    Private _tipoRisorsaCondivisa As Integer
	
	Public Property urlCondivisa() As String
        Get
            Return _urlCondivisa
        End Get
        Set(value As String)
            _urlCondivisa = value
        End Set
    End Property
    Private _urlCondivisa As String
	
	Public Property startDateValidation() As String
        Get
            Return _startDateValidation
        End Get
        Set(value As String)
            _startDateValidation = value
        End Set
    End Property
    Private _startDateValidation As String
	
	Public Property endDateValidation() As String
        Get
            Return _endDateValidation
        End Get
        Set(value As String)
            _endDateValidation = value
        End Set
    End Property
    Private _endDateValidation As String
	
	Public Property ack() As Integer
        Get
            Return _ack
        End Get
        Set(value As Integer)
            _ack = value
        End Set
    End Property
    Private _ack As Integer
	
End Class
