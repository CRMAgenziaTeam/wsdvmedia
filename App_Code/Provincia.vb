﻿Imports Microsoft.VisualBasic

Public Class provincia

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property
    Private _id As String

    Public Property sigla() As String
        Get
            Return _sigla
        End Get
        Set(value As String)
            _sigla = value
        End Set
    End Property
    Private _sigla As String

End Class
