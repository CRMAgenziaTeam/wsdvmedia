﻿Imports Microsoft.VisualBasic

Public Class GraphDataCRM

	Public Property period() As String
        Get
            Return _period
        End Get
        Set(value As String)
            _period = value
        End Set
    End Property
    Private _period As String
	
	Public Property idTipoAppuntamento() As Integer
        Get
            Return _idTipoAppuntamento
        End Get
        Set(value As Integer)
            _idTipoAppuntamento = value
        End Set
    End Property
    Private _idTipoAppuntamento As Integer
	
	Public Property fonte() As Integer
        Get
            Return _fonte
        End Get
        Set(value As Integer)
            _fonte = value
        End Set
    End Property
    Private _fonte As Integer
	
	Public Property num() As Integer
        Get
            Return _num
        End Get
        Set(value As Integer)
            _num = value
        End Set
    End Property
    Private _num As Integer
	
	Public Property midia() As String
        Get
            Return _midia
        End Get
        Set(value As String)
            _midia = value
        End Set
    End Property
    Private _midia As String
	
End Class
