﻿Public Class Agenda

    Public Property idAppuntamento() As String
        Get
            Return _idAppuntamento
        End Get
        Set(value As String)
            _idAppuntamento = value
        End Set
    End Property
    Private _idAppuntamento As String

    Public Property offerta() As String
        Get
            Return _Offerta
        End Get
        Set(value As String)
            _Offerta = value
        End Set
    End Property
    Private _Offerta As String
	
	Public Property filiale() As String
        Get
            Return _filiale
        End Get
        Set(value As String)
            _filiale = value
        End Set
    End Property
    Private _filiale As String

    Public Property contatto() As String
        Get
            Return _contatto
        End Get
        Set(value As String)
            _contatto = value
        End Set
    End Property
    Private _contatto As String

    Public Property tel_contatto() As String
        Get
            Return _tel_contatto
        End Get
        Set(value As String)
            _tel_contatto = value
        End Set
    End Property
    Private _tel_contatto As String

    Public Property cel_contatto() As String
        Get
            Return _cel_contatto
        End Get
        Set(value As String)
            _cel_contatto = value
        End Set
    End Property
    Private _cel_contatto As String

    Public Property proprietario() As String
        Get
            Return _proprietario
        End Get
        Set(value As String)
            _proprietario = value
        End Set
    End Property
    Private _proprietario As String

    Public Property tel_proprietario() As String
        Get
            Return _tel_proprietario
        End Get
        Set(value As String)
            _tel_proprietario = value
        End Set
    End Property
    Private _tel_proprietario As String

    Public Property cel_proprietario() As String
        Get
            Return _cel_proprietario
        End Get
        Set(value As String)
            _cel_proprietario = value
        End Set
    End Property
    Private _cel_proprietario As String

    Public Property email_proprietario() As String
        Get
            Return _email_proprietario
        End Get
        Set(value As String)
            _email_proprietario = value
        End Set
    End Property
    Private _email_proprietario As String
	
	Public Property note_proprietario() As String
        Get
            Return _note_proprietario
        End Get
        Set(value As String)
            _note_proprietario = value
        End Set
    End Property
    Private _note_proprietario As String


    Public Property note_appuntamento() As String
        Get
            Return _note_appuntamento
        End Get
        Set(value As String)
            _note_appuntamento = value
        End Set
    End Property
    Private _note_appuntamento As String


    Public Property data_appuntamento() As String
        Get
            Return _data_appuntamento
        End Get
        Set(value As String)
            _data_appuntamento = value
        End Set
    End Property
    Private _data_appuntamento As String

    Public Property consulente() As String
        Get
            Return _consulente
        End Get
        Set(value As String)
            _consulente = value
        End Set
    End Property
    Private _consulente As String

    Public Property orario_appuntamento() As String
        Get
            Return _orario_appuntamento
        End Get
        Set(value As String)
            _orario_appuntamento = value
        End Set
    End Property
    Private _orario_appuntamento As String

    Public Property conferma_contatto() As String
        Get
            Return _conferma_contatto
        End Get
        Set(value As String)
            _conferma_contatto = value
        End Set
    End Property
    Private _conferma_contatto As String

    Public Property conferma_proprietario() As String
        Get
            Return _conferma_proprietario
        End Get
        Set(value As String)
            _conferma_proprietario = value
        End Set
    End Property
    Private _conferma_proprietario As String

    Public Property valore_offerta() As String
        Get
            Return _valore_offerta
        End Get
        Set(value As String)
            _valore_offerta = value
        End Set
    End Property
    Private _valore_offerta As String

    Public Property indirizzo_offerta() As String
        Get
            Return _indirizzo_offerta
        End Get
        Set(value As String)
            _indirizzo_offerta = value
        End Set
    End Property
    Private _indirizzo_offerta As String

    Public Property area_offerta() As String
        Get
            Return _area_offerta
        End Get
        Set(value As String)
            _area_offerta = value
        End Set
    End Property
    Private _area_offerta As String

    Public Property zona_offerta() As String
        Get
            Return _zona_offerta
        End Get
        Set(value As String)
            _zona_offerta = value
        End Set
    End Property
    Private _zona_offerta As String

    Public Property esito_appuntamento() As String
        Get
            Return _esito_appuntamento
        End Get
        Set(value As String)
            _esito_appuntamento = value
        End Set
    End Property
    Private _esito_appuntamento As String

    Public Property id_esito_appuntamento() As String
        Get
            Return _id_esito_appuntamento
        End Get
        Set(value As String)
            _id_esito_appuntamento = value
        End Set
    End Property
    Private _id_esito_appuntamento As String

    Public Property tipo_appuntamento() As String
        Get
            Return _tipo_appuntamento
        End Get
        Set(value As String)
            _tipo_appuntamento = value
        End Set
    End Property
    Private _tipo_appuntamento As String

    Public Property id_tipo_appuntamento() As String
        Get
            Return _id_tipo_appuntamento
        End Get
        Set(value As String)
            _id_tipo_appuntamento = value
        End Set
    End Property
    Private _id_tipo_appuntamento As String

    Public Property id_anagrafica() As String
        Get
            Return _id_anagrafica
        End Get
        Set(value As String)
            _id_anagrafica = value
        End Set
    End Property
    Private _id_anagrafica As String


    Public Property colore() As String
        Get
            Return _colore
        End Get
        Set(value As String)
            _colore = value
        End Set
    End Property
    Private _colore As String

    Public Property idScheda_offerta() As String
        Get
            Return _idScheda_offerta
        End Get
        Set(value As String)
            _idScheda_offerta = value
        End Set
    End Property
    Private _idScheda_offerta As String

    Public Property area() As String
        Get
            Return _area
        End Get
        Set(value As String)
            _area = value
        End Set
    End Property
    Private _area As String

    Public Property codice() As String
        Get
            Return _codice
        End Get
        Set(value As String)
            _codice = value
        End Set
    End Property
    Private _codice As String

    Public Property tipologia() As String
        Get
            Return _tipologia
        End Get
        Set(value As String)
            _tipologia = value
        End Set
    End Property
    Private _tipologia As String

    Public Property prezzo() As String
        Get
            Return _prezzo
        End Get
        Set(value As String)
            _prezzo = value
        End Set
    End Property
    Private _prezzo As String

    Public Property mq() As String
        Get
            Return _mq
        End Get
        Set(value As String)
            _mq = value
        End Set
    End Property
    Private _mq As String

    Public Property camere() As String
        Get
            Return _camere
        End Get
        Set(value As String)
            _camere = value
        End Set
    End Property
    Private _camere As String

    Public Property locali() As String
        Get
            Return _locali
        End Get
        Set(value As String)
            _locali = value
        End Set
    End Property
    Private _locali As String

    Public Property latitudine() As String
        Get
            Return _latitudine
        End Get
        Set(value As String)
            _latitudine = value
        End Set
    End Property
    Private _latitudine As String

    Public Property longitudine() As String
        Get
            Return _longitudine
        End Get
        Set(value As String)
            _longitudine = value
        End Set
    End Property
    Private _longitudine As String

    Public Property telefono_referente() As String
        Get
            Return _telefono_referente
        End Get
        Set(value As String)
            _telefono_referente = value
        End Set
    End Property
    Private _telefono_referente As String

    Public Property indirizzo_referente() As String
        Get
            Return _indirizzo_referente
        End Get
        Set(value As String)
            _indirizzo_referente = value
        End Set
    End Property
    Private _indirizzo_referente As String

    Public Property email_referente() As String
        Get
            Return _email_referente
        End Get
        Set(value As String)
            _email_referente = value
        End Set
    End Property
    Private _email_referente As String

    Public Property foto_link() As String
        Get
            Return _foto_link
        End Get
        Set(value As String)
            _foto_link = value
        End Set
    End Property
    Private _foto_link As String

    Public Property email_contatto() As String
        Get
            Return _email_contatto
        End Get
        Set(value As String)
            _email_contatto = value
        End Set
    End Property
    Private _email_contatto As String
	
	Public Property note_contatto() As String
        Get
            Return _note_contatto
        End Get
        Set(value As String)
            _note_contatto = value
        End Set
    End Property
    Private _note_contatto As String
	
	Public Property note_richiesta() As String
        Get
            Return _note_richiesta
        End Get
        Set(value As String)
            _note_richiesta = value
        End Set
    End Property
    Private _note_richiesta As String

    Public Property sito_referente() As String
        Get
            Return _sito_referente
        End Get
        Set(value As String)
            _sito_referente = value
        End Set
    End Property
    Private _sito_referente As String

    Public Property bagni() As String
        Get
            Return _bagni
        End Get
        Set(value As String)
            _bagni = value
        End Set
    End Property
    Private _bagni As String

    Public Property posto_auto() As String
        Get
            Return _Posto_auto
        End Get
        Set(value As String)
            _Posto_auto = value
        End Set
    End Property
    Private _Posto_auto As String

    Public Property zona() As String
        Get
            Return _zona
        End Get
        Set(value As String)
            _zona = value
        End Set
    End Property
    Private _zona As String

    Public Property classe_energetica() As String
        Get
            Return _classe_energetica
        End Get
        Set(value As String)
            _classe_energetica = value
        End Set
    End Property
    Private _classe_energetica As String

    Public Property descrizione() As String
        Get
            Return _descrizione
        End Get
        Set(value As String)
            _descrizione = value
        End Set
    End Property
    Private _descrizione As String

    'Public Property info() As String
    '    Get
    '        Return _info
    '    End Get
    '    Set(value As String)
    '        _info = value
    '    End Set
    'End Property
    'Private _info As String

    'Public Property info_completamento() As String
    '    Get
    '        Return _info_completamento
    '    End Get
    '    Set(value As String)
    '        _info_completamento = value
    '    End Set
    'End Property
    'Private _info_completamento As String

    Public Property pertinenze() As String
        Get
            Return _pertinenze
        End Get
        Set(value As String)
            _pertinenze = value
        End Set
    End Property
    Private _pertinenze As String

    Public Property impianti() As String
        Get
            Return _impianti
        End Get
        Set(value As String)
            _impianti = value
        End Set
    End Property
    Private _impianti As String

    Public Property pavimenti() As String
        Get
            Return _pavimenti
        End Get
        Set(value As String)
            _pavimenti = value
        End Set
    End Property
    Private _pavimenti As String

    Public Property accessori() As String
        Get
            Return _accessori
        End Get
        Set(value As String)
            _accessori = value
        End Set
    End Property
    Private _accessori As String

    Public Property servizi() As String
        Get
            Return _servizi
        End Get
        Set(value As String)
            _servizi = value
        End Set
    End Property
    Private _servizi As String

    Public Property foto1() As String
        Get
            Return _foto1
        End Get
        Set(value As String)
            _foto1 = value
        End Set
    End Property
    Private _foto1 As String

    Public Property foto2() As String
        Get
            Return _foto2
        End Get
        Set(value As String)
            _foto2 = value
        End Set
    End Property
    Private _foto2 As String

    Public Property foto3() As String
        Get
            Return _foto3
        End Get
        Set(value As String)
            _foto3 = value
        End Set
    End Property
    Private _foto3 As String

    Public Property foto4() As String
        Get
            Return _foto4
        End Get
        Set(value As String)
            _foto4 = value
        End Set
    End Property
    Private _foto4 As String

    Public Property foto5() As String
        Get
            Return _foto5
        End Get
        Set(value As String)
            _foto5 = value
        End Set
    End Property
    Private _foto5 As String

    Public Property foto6() As String
        Get
            Return _foto6
        End Get
        Set(value As String)
            _foto6 = value
        End Set
    End Property
    Private _foto6 As String

    Public Property foto7() As String
        Get
            Return _foto7
        End Get
        Set(value As String)
            _foto7 = value
        End Set
    End Property
    Private _foto7 As String

    Public Property foto8() As String
        Get
            Return _foto8
        End Get
        Set(value As String)
            _foto8 = value
        End Set
    End Property
    Private _foto8 As String

    Public Property foto9() As String
        Get
            Return _foto9
        End Get
        Set(value As String)
            _foto9 = value
        End Set
    End Property
    Private _foto9 As String

    Public Property foto10() As String
        Get
            Return _foto10
        End Get
        Set(value As String)
            _foto10 = value
        End Set
    End Property
    Private _foto10 As String

    Public Property affinity() As String
        Get
            Return _affinity
        End Get
        Set(value As String)
            _affinity = value
        End Set
    End Property
    Private _affinity As String

    Public Property indirizzo() As String
        Get
            Return _indirizzo
        End Get
        Set(value As String)
            _indirizzo = value
        End Set
    End Property
    Private _indirizzo As String

    Public Property urlVideo() As String
        Get
            Return _urlVideo
        End Get
        Set(value As String)
            _urlVideo = value
        End Set
    End Property
    Private _urlVideo As String

    Public Property urlFotoReferente() As String
        Get
            Return _urlFotoReferente
        End Get
        Set(value As String)
            _urlFotoReferente = value
        End Set
    End Property
    Private _urlFotoReferente As String

    Public Property comune() As String
        Get
            Return _comune
        End Get
        Set(value As String)
            _comune = value
        End Set
    End Property
    Private _comune As String

    Public Property idcomune() As String
        Get
            Return _idcomune
        End Get
        Set(value As String)
            _idcomune = value
        End Set
    End Property
    Private _idcomune As String

    Public Property spese_extra() As String
        Get
            Return _spese_extra
        End Get
        Set(value As String)
            _spese_extra = value
        End Set
    End Property
    Private _spese_extra As String

    Public Property spese_sospeso() As String
        Get
            Return _spese_sospeso
        End Get
        Set(value As String)
            _spese_sospeso = value
        End Set
    End Property
    Private _spese_sospeso As String

    Public Property valore_mercato() As String
        Get
            Return _valore_mercato
        End Get
        Set(value As String)
            _valore_mercato = value
        End Set
    End Property
    Private _valore_mercato As String

    Public Property idscheda() As String
        Get
            Return _id_scheda
        End Get
        Set(value As String)
            _id_scheda = value
        End Set
    End Property
    Private _id_scheda As String

    Public Property epoca() As String
        Get
            Return _epoca
        End Get
        Set(value As String)
            _epoca = value
        End Set
    End Property
    Private _epoca As String

    Public Property Livellistabile() As String
        Get
            Return _Livellistabile
        End Get
        Set(value As String)
            _Livellistabile = value
        End Set
    End Property
    Private _Livellistabile As String

    Public Property Durata() As String
        Get
            Return _Durata
        End Get
        Set(value As String)
            _Durata = value
        End Set
    End Property
    Private _Durata As String

    Public Property Nome_contatto() As String
        Get
            Return _Nome_contatto
        End Get
        Set(value As String)
            _Nome_contatto = value
        End Set
    End Property
    Private _Nome_contatto As String

    Public Property Cognome_contatto() As String
        Get
            Return _Cognome_contatto
        End Get
        Set(value As String)
            _Cognome_contatto = value
        End Set
    End Property
    Private _Cognome_contatto As String

	
	Public Property portalePromozione() As String
        Get
            Return _portalePromozione
        End Get
        Set(value As String)
            _portalePromozione = value
        End Set
    End Property
    Private _portalePromozione As String
	
	
	Public Property modPag() As String
        Get
            Return _modPag
        End Get
        Set(value As String)
            _modPag = value
        End Set
    End Property
    Private _modPag As String
	
    Public Property foto11() As String
        Get
            Return _foto11
        End Get
        Set(value As String)
            _foto11 = value
        End Set
    End Property
    Private _foto11 As String

    Public Property foto12() As String
        Get
            Return _foto12
        End Get
        Set(value As String)
            _foto12 = value
        End Set
    End Property
    Private _foto12 As String

    Public Property foto13() As String
        Get
            Return _foto13
        End Get
        Set(value As String)
            _foto13 = value
        End Set
    End Property
    Private _foto13 As String

    Public Property foto14() As String
        Get
            Return _foto14
        End Get
        Set(value As String)
            _foto14 = value
        End Set
    End Property
    Private _foto14 As String

    Public Property foto15() As String
        Get
            Return _foto15
        End Get
        Set(value As String)
            _foto15 = value
        End Set
    End Property
    Private _foto15 As String

    Public Property foto16() As String
        Get
            Return _foto16
        End Get
        Set(value As String)
            _foto16 = value
        End Set
    End Property
    Private _foto16 As String

    Public Property foto17() As String
        Get
            Return _foto17
        End Get
        Set(value As String)
            _foto17 = value
        End Set
    End Property
    Private _foto17 As String

    Public Property foto18() As String
        Get
            Return _foto18
        End Get
        Set(value As String)
            _foto18 = value
        End Set
    End Property
    Private _foto18 As String

    Public Property foto19() As String
        Get
            Return _foto19
        End Get
        Set(value As String)
            _foto19 = value
        End Set
    End Property
    Private _foto19 As String

    Public Property foto20() As String
        Get
            Return _foto20
        End Get
        Set(value As String)
            _foto20 = value
        End Set
    End Property
    Private _foto20 As String
	
	Public Property delegato_contatto() As String
        Get
            Return _delegato_contatto
        End Get
        Set(value As String)
            _delegato_contatto = value
        End Set
    End Property
    Private _delegato_contatto As String
	
	Public Property successivo() As String
        Get
            Return _successivo
        End Get
        Set(value As String)
            _successivo = value
        End Set
    End Property
    Private _successivo As String


End Class